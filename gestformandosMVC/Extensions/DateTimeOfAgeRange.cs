﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Helper
{
    public class DateTimeOfAgeRange : ValidationAttribute
    {
        public string MinimumDate { get; set; }
        public string MaximumDate { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime minDate = DateTime.Parse(MinimumDate ?? "01/01/1900");
            DateTime maxDate = DateTime.Parse(MaximumDate ?? DateTime.Now.AddYears(-18).ToShortDateString());

            if ((DateTime)value >= minDate && (DateTime)value <= maxDate)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Date is not within range.");
            }
        }
    }
}