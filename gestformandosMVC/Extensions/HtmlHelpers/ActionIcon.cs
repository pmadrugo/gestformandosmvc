﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace gestformandosMVC.Extensions.HtmlHelpers
{
    public static class ActionIconExtensions
    {
        public static IHtmlString ActionIcon(this HtmlHelper helper, string iconClass, string action, object routeValues)
        {
            var aBuilder = new TagBuilder("a");

            aBuilder.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(action, routeValues));

            aBuilder.InnerHtml = $"<i class='fas fa-{iconClass}'></i>";

            return new HtmlString(aBuilder.ToString());
        }

        public static IHtmlString ActionIcon(this HtmlHelper helper, string iconClass, string action, object routeValues, object htmlAttributes)
        {
            var aBuilder = new TagBuilder("a");

            aBuilder.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(action, routeValues));

            aBuilder.InnerHtml = $"<i class='fas fa-{iconClass}'></i>";
            aBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return new HtmlString(aBuilder.ToString());
        }

        public static IHtmlString ActionIcon(this HtmlHelper helper, string iconClass, string action, string text, object routeValues, object htmlAttributes, bool printText = false)
        {
            var aBuilder = new TagBuilder("a");

            aBuilder.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(action, routeValues));

            if (printText)
            {
                aBuilder.InnerHtml = $"<i class='fas fa-{iconClass}'></i>&nbsp;<span>{text}</span>";
            }
            else
            {
                aBuilder.InnerHtml = $"<i class='fas fa-{iconClass}'></i>";
                aBuilder.MergeAttribute("title", text);
            }

            aBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return new HtmlString($"<div class='actionIcon'>{aBuilder.ToString()}</div>");
        }

        public static IHtmlString ActionIcon(this HtmlHelper helper, string iconClass, string action, string text, object htmlAttributes, bool printText = false)
        {
            var aBuilder = new TagBuilder("a");

            aBuilder.MergeAttribute("href", new UrlHelper(helper.ViewContext.RequestContext).Action(action));
            aBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            if (printText)
            {
                aBuilder.InnerHtml = $"<i class='fas fa-{iconClass}'></i>&nbsp;<span>{text}</span>";
            }
            else
            {
                aBuilder.InnerHtml = $"<i class='fas fa-{iconClass}'></i>";
                aBuilder.MergeAttribute("title", text);
            }

            return new HtmlString(aBuilder.ToString());
        }
    }
}