﻿using gestformandosMVC.Data.Repository;
using gestformandosMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace gestformandosMVC.Extensions.HtmlHelpers
{
    public static class UserProfileExtensions
    {
        public static IHtmlString CurrentUserProfileName(this HtmlHelper helper)
        {
            if (HttpContext.Current.User.IsInRole("SuperUser"))
            {
                return new HtmlString("SuperUser");
            }

            using (var profileRepo = new ProfileRepository())
            { 
                var profile = profileRepo.GetProfileByUserID(HttpContext.Current.User.Identity.GetUserId());
                if (profile != null)
                {
                    return new HtmlString(profile.FullName);
                }
            }

            return new HtmlString("Convidado");
        }

        public static IHtmlString GetCurrentUserProfessorNumber(this HtmlHelper helper)
        {
            if (HttpContext.Current.User.IsInRole("SuperUser"))
            {
                return new HtmlString("N/A");
            }

            using (var profileRepo = new ProfileRepository())
            {
                var profile = profileRepo.GetProfileByUserID(HttpContext.Current.User.Identity.GetUserId());
                if (profile != null)
                {
                    var professor = profileRepo.GetProfileProfessor(profile.Id);
                    if (professor != null)
                    {
                        return new HtmlString(professor.Id.ToString());
                    }
                }
                
            }

            return new HtmlString("N/A");
        }

        public static IHtmlString GetCurrentUserAdminNumber(this HtmlHelper helper)
        {
            if (HttpContext.Current.User.IsInRole("SuperUser"))
            {
                return new HtmlString("N/A");
            }

            using (var profileRepo = new ProfileRepository())
            {
                var profile = profileRepo.GetProfileByUserID(HttpContext.Current.User.Identity.GetUserId());
                if (profile != null)
                {
                    var admin = profileRepo.GetProfileAdministration(profile.Id);
                    if (admin != null)
                    {
                        return new HtmlString(admin.Id.ToString());
                    }
                }

            }

            return new HtmlString("N/A");
        }

        public static IHtmlString GetCurrentUserStudentNumber(this HtmlHelper helper)
        {
            if (HttpContext.Current.User.IsInRole("SuperUser"))
            {
                return new HtmlString("N/A");
            }

            using (var profileRepo = new ProfileRepository())
            {
                var profile = profileRepo.GetProfileByUserID(HttpContext.Current.User.Identity.GetUserId());
                if (profile != null)
                {
                    var student = profileRepo.GetProfileStudent(profile.Id);
                    if (student != null)
                    {
                        return new HtmlString(student.Id.ToString());
                    }
                }

            }

            return new HtmlString("N/A");
        }
    }
}