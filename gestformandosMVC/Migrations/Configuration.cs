namespace gestformandosMVC.Migrations
{
    using gestformandosMVC.Data.DatabaseContexts;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<GestFormandosDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(GestFormandosDataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            #region Default Genders
            context.Genders.AddOrUpdate(new Data.Entities.Gender() { Id = 1, Name = "Masculino" });
            context.Genders.AddOrUpdate(new Data.Entities.Gender() { Id = 2, Name = "Feminino" });
            #endregion

            #region Default Countries
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 1, Name = "Afeganist�o", Citizenship = "Afeg�a", Nationality = "Afeg�a" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 2, Name = "�frica do Sul", Citizenship = "Sul-africana", Nationality = "Sul-africana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 3, Name = "Alb�nia", Citizenship = "Albanesa", Nationality = "Albanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 4, Name = "Alemanha", Citizenship = "Alem�", Nationality = "Alem�" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 5, Name = "Andorra", Citizenship = "Andorrana", Nationality = "Andorrana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 6, Name = "Angola", Citizenship = "Angolana", Nationality = "Angolana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 7, Name = "Anguilla", Citizenship = "Anguilana", Nationality = "Anguilana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 8, Name = "Ant�gua e Barbuda", Citizenship = "Antiguana", Nationality = "Antiguana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 9, Name = "Antilhas Holandesas", Citizenship = "Antilhana", Nationality = "Antilhana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 10, Name = "Ar�bia Saudita", Citizenship = "�rabe-saudita", Nationality = "�rabe-saudita" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 11, Name = "Arg�lia", Citizenship = "Argelina", Nationality = "Argelina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 12, Name = "Argentina", Citizenship = "Argentina", Nationality = "Argentina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 13, Name = "Arm�nia", Citizenship = "Arm�nia", Nationality = "Arm�nia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 14, Name = "Aruba", Citizenship = "Arubana", Nationality = "Arubana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 15, Name = "Austr�lia", Citizenship = "Australiana", Nationality = "Australiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 16, Name = "�ustria", Citizenship = "Austr�aca", Nationality = "Austr�aca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 17, Name = "Azerbaij�o", Citizenship = "Azeri", Nationality = "Azeri" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 18, Name = "Bahamas", Citizenship = "Baamiana", Nationality = "Baamiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 19, Name = "Bahrein", Citizenship = "Barenita", Nationality = "Barenita" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 20, Name = "Bangladesh", Citizenship = "Bangladechiana", Nationality = "Bangladechiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 21, Name = "Barbados", Citizenship = "Barbadense", Nationality = "Barbadense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 22, Name = "B�lgica", Citizenship = "Belga", Nationality = "Belga" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 23, Name = "Belize", Citizenship = "Belizenha", Nationality = "Belizenha" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 24, Name = "Benim", Citizenship = "Beninesea", Nationality = "Beninesea" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 25, Name = "Bermudas", Citizenship = "Bermudense", Nationality = "Bermudense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 26, Name = "Bielorr�ssia", Citizenship = "Bielorrussa", Nationality = "Bielorrussa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 27, Name = "Bol�via", Citizenship = "Boliviana", Nationality = "Boliviana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 28, Name = "B�snia e Herzegovina", Citizenship = "B�snia", Nationality = "B�snia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 29, Name = "Botswana", Citizenship = "Botsuana", Nationality = "Botsuana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 30, Name = "Brasil", Citizenship = "Brasileira", Nationality = "Brasileira" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 31, Name = "Brunei", Citizenship = "Brune�na", Nationality = "Brune�na" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 32, Name = "Bulg�ria", Citizenship = "B�lgara", Nationality = "B�lgara" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 33, Name = "Burkina Faso", Citizenship = "Burquina", Nationality = "Burquina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 34, Name = "Burundi", Citizenship = "Burundiana", Nationality = "Burundiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 35, Name = "But�o", Citizenship = "Butanesa", Nationality = "Butanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 36, Name = "Cabo Verde", Citizenship = "Cabo-verdiana", Nationality = "Cabo-verdiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 37, Name = "Camar�es", Citizenship = "Camaronesa", Nationality = "Camaronesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 38, Name = "Camboja", Citizenship = "Cambojana", Nationality = "Cambojana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 39, Name = "Canad�", Citizenship = "Canadiana", Nationality = "Canadiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 40, Name = "Cazaquist�o", Citizenship = "Cazaque", Nationality = "Cazaque" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 41, Name = "Chade", Citizenship = "Chadiana", Nationality = "Chadiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 42, Name = "Chile", Citizenship = "Chilena", Nationality = "Chilena" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 43, Name = "China", Citizenship = "China", Nationality = "China" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 44, Name = "Chipre", Citizenship = "Cipriota", Nationality = "Cipriota" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 45, Name = "Col�mbia", Citizenship = "Colombiana", Nationality = "Colombiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 46, Name = "Comores", Citizenship = "Comoriana", Nationality = "Comoriana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 47, Name = "Rep�blica do Congo", Citizenship = "Congolesa", Nationality = "Congolesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 48, Name = "Coreia do Norte", Citizenship = "Norte-coreana", Nationality = "Norte-coreana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 49, Name = "Coreia do Sul", Citizenship = "Sul-coreana", Nationality = "Sul-coreana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 50, Name = "Costa do Marfim", Citizenship = "Marfinense", Nationality = "Marfinense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 51, Name = "Costa Rica", Citizenship = "Costarriquenha", Nationality = "Costarriquenha" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 52, Name = "Cro�cia", Citizenship = "Croata", Nationality = "Croata" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 53, Name = "Cuba", Citizenship = "Cubana", Nationality = "Cubana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 54, Name = "Cura�ao", Citizenship = "Cura�auense", Nationality = "Cura�auense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 55, Name = "Dinamarca", Citizenship = "Dinamarqua", Nationality = "Dinamarqua" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 56, Name = "Djibouti", Citizenship = "Jibutiana", Nationality = "Jibutiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 57, Name = "Dominica", Citizenship = "Dominiqua", Nationality = "Dominiqua" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 58, Name = "Egito", Citizenship = "Eg�pcia", Nationality = "Eg�pcia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 59, Name = "El Salvador", Citizenship = "Salvadorenha", Nationality = "Salvadorenha" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 60, Name = "Emirados �rabes", Citizenship = "Emiradense", Nationality = "Emiradense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 61, Name = "Equador", Citizenship = "Equatoriana", Nationality = "Equatoriana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 62, Name = "Eritreia", Citizenship = "Eritreia", Nationality = "Eritreia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 63, Name = "Esc�cia", Citizenship = "Escocesa", Nationality = "Escocesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 64, Name = "Eslov�quia", Citizenship = "Eslovaca", Nationality = "Eslovaca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 65, Name = "Eslov�nia", Citizenship = "Eslovena", Nationality = "Eslovena" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 66, Name = "Espanha", Citizenship = "Espanhola", Nationality = "Espanhola" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 67, Name = "Estados Federados da Micron�sia", Citizenship = "Micron�sia", Nationality = "Micron�sia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 68, Name = "Estados Unidos", Citizenship = "Norte-americana", Nationality = "Norte-americana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 69, Name = "Est�nia", Citizenship = "Est�nia", Nationality = "Est�nia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 70, Name = "Eti�pia", Citizenship = "Et�ope", Nationality = "Et�ope" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 71, Name = "Fiji", Citizenship = "Fijiana", Nationality = "Fijiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 72, Name = "Filipinas", Citizenship = "Filipino", Nationality = "Filipino" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 73, Name = "Finl�ndia", Citizenship = "Finlandesa", Nationality = "Finlandesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 74, Name = "Fran�a", Citizenship = "Francesa", Nationality = "Francesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 75, Name = "Gab�o", Citizenship = "Gabonesa", Nationality = "Gabonesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 76, Name = "G�mbia", Citizenship = "Gambiana", Nationality = "Gambiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 77, Name = "Gana", Citizenship = "Ganesa", Nationality = "Ganesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 78, Name = "Ge�rgia", Citizenship = "Georgiana", Nationality = "Georgiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 79, Name = "Granada", Citizenship = "Granadina", Nationality = "Granadina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 80, Name = "Gr�cia", Citizenship = "Grega", Nationality = "Grega" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 81, Name = "Guadalupe", Citizenship = "Guadalupense", Nationality = "Guadalupense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 82, Name = "Guam", Citizenship = "Guamesa", Nationality = "Guamesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 83, Name = "Guatemala", Citizenship = "Guatemalteca", Nationality = "Guatemalteca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 84, Name = "Guiana", Citizenship = "Guianesa", Nationality = "Guianesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 85, Name = "Guiana Francesa", Citizenship = "Guianense", Nationality = "Guianense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 86, Name = "Guin�", Citizenship = "Guineana", Nationality = "Guineana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 87, Name = "Guin� Equatorial", Citizenship = "Guin�u-equatoriana", Nationality = "Guin�u-equatoriana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 88, Name = "Guin�-Bissau", Citizenship = "Guineense", Nationality = "Guineense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 89, Name = "Haiti", Citizenship = "Haitiana", Nationality = "Haitiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 90, Name = "Honduras", Citizenship = "Hondurenha", Nationality = "Hondurenha" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 91, Name = "Hong Kong", Citizenship = "Honconguesa", Nationality = "Honconguesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 92, Name = "Hungria", Citizenship = "H�ngara", Nationality = "H�ngara" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 93, Name = "I�men", Citizenship = "Iemenita", Nationality = "Iemenita" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 94, Name = "Ilhas Cayman", Citizenship = "Caimanesa", Nationality = "Caimanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 95, Name = "Ilhas Cook", Citizenship = "Cookense", Nationality = "Cookense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 96, Name = "Ilhas Fero�", Citizenship = "Faroense", Nationality = "Faroense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 97, Name = "Ilhas Salom�o", Citizenship = "Salomonense", Nationality = "Salomonense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 98, Name = "Ilhas Virgens Americanas", Citizenship = "Virginense", Nationality = "Virginense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 99, Name = "Ilhas Virgens Brit�nicas", Citizenship = "Virginense", Nationality = "Virginense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 100, Name = "�ndia", Citizenship = "Indiana", Nationality = "Indiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 101, Name = "Indon�sia", Citizenship = "Indon�sia", Nationality = "Indon�sia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 102, Name = "Inglaterra", Citizenship = "Inglesa", Nationality = "Inglesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 103, Name = "Ir�o", Citizenship = "Iraniana", Nationality = "Iraniana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 104, Name = "Iraque", Citizenship = "Iraquiana", Nationality = "Iraquiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 105, Name = "Irlanda", Citizenship = "Irlandesa", Nationality = "Irlandesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 106, Name = "Irlanda do Norte", Citizenship = "Norte-irlandesa", Nationality = "Norte-irlandesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 107, Name = "Isl�ndia", Citizenship = "Islanda", Nationality = "Islanda" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 108, Name = "Israel", Citizenship = "Israelita", Nationality = "Israelita" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 109, Name = "It�lia", Citizenship = "Italiana", Nationality = "Italiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 110, Name = "Jamaica", Citizenship = "Jamaicana", Nationality = "Jamaicana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 111, Name = "Jap�o", Citizenship = "Japonesa", Nationality = "Japonesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 112, Name = "Jord�nia", Citizenship = "Jordana", Nationality = "Jordana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 113, Name = "Kiribati", Citizenship = "Quiribatiana", Nationality = "Quiribatiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 114, Name = "Kosovo", Citizenship = "Kosovar", Nationality = "Kosovar" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 115, Name = "Kuwait", Citizenship = "Kuwaitiana", Nationality = "Kuwaitiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 116, Name = "Laos", Citizenship = "Laociana", Nationality = "Laociana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 117, Name = "Lesoto", Citizenship = "Lesotiana", Nationality = "Lesotiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 118, Name = "Let�nia", Citizenship = "Let�", Nationality = "Let�" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 119, Name = "L�bano", Citizenship = "Libanesa", Nationality = "Libanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 120, Name = "Lib�ria", Citizenship = "Liberiana", Nationality = "Liberiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 121, Name = "L�bia", Citizenship = "L�bia", Nationality = "L�bia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 122, Name = "Liechtenstein", Citizenship = "Listenstainiana", Nationality = "Listenstainiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 123, Name = "Litu�nia", Citizenship = "Lituana", Nationality = "Lituana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 124, Name = "Luxemburgo", Citizenship = "Luxemburguesa", Nationality = "Luxemburguesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 125, Name = "Macau", Citizenship = "Macaense", Nationality = "Macaense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 126, Name = "Maced�nia do Norte", Citizenship = "Maced�nica", Nationality = "Maced�nica" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 127, Name = "Madag�scar", Citizenship = "Malgaxe", Nationality = "Malgaxe" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 128, Name = "Mal�sia", Citizenship = "Malaia", Nationality = "Malaia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 129, Name = "Malawi", Citizenship = "Malauiana", Nationality = "Malauiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 130, Name = "Maldivas", Citizenship = "Maldiva", Nationality = "Maldiva" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 131, Name = "Mali", Citizenship = "Maliano", Nationality = "Maliano" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 132, Name = "Malta", Citizenship = "Maltesa", Nationality = "Maltesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 133, Name = "Marrocos", Citizenship = "Marroquina", Nationality = "Marroquina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 134, Name = "Martinica", Citizenship = "Martinicana", Nationality = "Martinicana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 135, Name = "Maur�cia", Citizenship = "Mauriciana", Nationality = "Mauriciana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 136, Name = "Maurit�nia", Citizenship = "Mauritana", Nationality = "Mauritana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 137, Name = "M�xico", Citizenship = "Mexicana", Nationality = "Mexicana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 138, Name = "Mianmar", Citizenship = "Birmanesa", Nationality = "Birmanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 139, Name = "Mo�ambique", Citizenship = "Mo�ambicana", Nationality = "Mo�ambicana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 140, Name = "Mold�via", Citizenship = "Moldava", Nationality = "Moldava" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 141, Name = "M�naco", Citizenship = "Monegasca", Nationality = "Monegasca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 142, Name = "Mong�lia", Citizenship = "Mongol", Nationality = "Mongol" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 143, Name = "Montenegro", Citizenship = "Montenegrina", Nationality = "Montenegrina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 144, Name = "Montserrat", Citizenship = "Monserratense", Nationality = "Monserratense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 145, Name = "Nam�bia", Citizenship = "Namibiana", Nationality = "Namibiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 146, Name = "Nauru", Citizenship = "Nauruana", Nationality = "Nauruana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 147, Name = "Nepal", Citizenship = "Nepalesa", Nationality = "Nepalesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 148, Name = "Nicar�gua", Citizenship = "Nicaraguense", Nationality = "Nicaraguense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 149, Name = "N�ger", Citizenship = "Nigerina", Nationality = "Nigerina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 150, Name = "Nig�ria", Citizenship = "Nigeriana", Nationality = "Nigeriana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 151, Name = "Noruega", Citizenship = "Norueguesa", Nationality = "Norueguesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 152, Name = "Nova Caled�nia", Citizenship = "Neocaled�nia", Nationality = "Neocaled�nia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 153, Name = "Nova Zel�ndia", Citizenship = "Neozelandesa", Nationality = "Neozelandesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 154, Name = "Om�", Citizenship = "Omanense", Nationality = "Omanense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 155, Name = "Pa�s de Gales", Citizenship = "Gala", Nationality = "Gala" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 156, Name = "Pa�ses Baixos", Citizenship = "Neerlandesa", Nationality = "Neerlandesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 157, Name = "Palau", Citizenship = "Palauana", Nationality = "Palauana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 158, Name = "Palestina", Citizenship = "Palestiniana", Nationality = "Palestiniana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 159, Name = "Panam�", Citizenship = "Panamenha", Nationality = "Panamenha" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 160, Name = "Papua-Nova Guin�", Citizenship = "Papua", Nationality = "Papua" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 161, Name = "Paquist�o", Citizenship = "Paquistanesa", Nationality = "Paquistanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 162, Name = "Paraguai", Citizenship = "Paraguaia", Nationality = "Paraguaia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 163, Name = "Peru", Citizenship = "Peruana", Nationality = "Peruana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 164, Name = "Polin�sia Francesa", Citizenship = "Polin�sia", Nationality = "Polin�sia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 165, Name = "Pol�nia", Citizenship = "Polaca", Nationality = "Polaca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 166, Name = "Porto Rico", Citizenship = "Porto-riquenha", Nationality = "Porto-riquenha" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 167, Name = "Portugal", Citizenship = "Portuguesa", Nationality = "Portuguesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 168, Name = "Catar", Citizenship = "Catariana", Nationality = "Catariana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 169, Name = "Qu�nia", Citizenship = "Queniana", Nationality = "Queniana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 170, Name = "Quirguist�o", Citizenship = "Quirguiz", Nationality = "Quirguiz" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 171, Name = "Reino Unido", Citizenship = "Brit�nica", Nationality = "Brit�nica" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 172, Name = "Rep�blica Centro-Africana", Citizenship = "Centro-africana", Nationality = "Centro-africana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 173, Name = "Rep�blica Checa", Citizenship = "Tcheca", Nationality = "Tcheca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 174, Name = "Taiwan", Citizenship = "Taiwanesa", Nationality = "Taiwanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 175, Name = "Rep�blica Democr�tica do Congo", Citizenship = "Congolesa", Nationality = "Congolesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 176, Name = "Rep�blica Dominicana", Citizenship = "Dominicana", Nationality = "Dominicana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 177, Name = "Rom�nia", Citizenship = "Romena", Nationality = "Romena" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 178, Name = "Ruanda", Citizenship = "Ruandesa", Nationality = "Ruandesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 179, Name = "R�ssia", Citizenship = "Russa", Nationality = "Russa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 180, Name = "S�o Martinho", Citizenship = "S�o-martinhense", Nationality = "S�o-martinhense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 181, Name = "Samoa", Citizenship = "Samoana", Nationality = "Samoana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 182, Name = "Samoa Americana", Citizenship = "Samoense", Nationality = "Samoense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 183, Name = "Santa L�cia", Citizenship = "Santa-luciense", Nationality = "Santa-luciense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 184, Name = "S�o Crist�v�o e N�vis", Citizenship = "S�o-cristovense", Nationality = "S�o-cristovense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 185, Name = "San Marino", Citizenship = "S�o-marinhense", Nationality = "S�o-marinhense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 186, Name = "S�o Tom� e Pr�ncipe", Citizenship = "S�o-tomense", Nationality = "S�o-tomense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 187, Name = "S�o Vicente e Granadinas", Citizenship = "S�o-vicentina", Nationality = "S�o-vicentina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 188, Name = "Senegal", Citizenship = "Senegalesa", Nationality = "Senegalesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 189, Name = "Serra Leoa", Citizenship = "Serra-leonesa", Nationality = "Serra-leonesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 190, Name = "S�rvia", Citizenship = "S�rvia", Nationality = "S�rvia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 191, Name = "Seicheles", Citizenship = "Seichelense", Nationality = "Seichelense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 192, Name = "Singapura", Citizenship = "Singapurense", Nationality = "Singapurense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 193, Name = "S�o Martinho", Citizenship = "S�o-martinhense", Nationality = "S�o-martinhense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 194, Name = "S�ria", Citizenship = "S�ria", Nationality = "S�ria" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 195, Name = "Som�lia", Citizenship = "Somali", Nationality = "Somali" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 196, Name = "Sri Lanka", Citizenship = "Cingalesa", Nationality = "Cingalesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 197, Name = "Suazil�ndia", Citizenship = "Suaziland�a", Nationality = "Suaziland�a" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 198, Name = "Sud�o", Citizenship = "Sudanesa", Nationality = "Sudanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 199, Name = "Su�cia", Citizenship = "Sueca", Nationality = "Sueca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 200, Name = "Su��a", Citizenship = "Su��a", Nationality = "Su��a" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 201, Name = "Suriname", Citizenship = "Surinamesa", Nationality = "Surinamesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 202, Name = "Tajiquist�o", Citizenship = "Tajique", Nationality = "Tajique" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 203, Name = "Tail�ndia", Citizenship = "Tailand�s", Nationality = "Tailand�s" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 204, Name = "Taip� Chinesa", Citizenship = "Taiwanesa", Nationality = "Taiwanesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 205, Name = "Tanz�nia", Citizenship = "Tanzaniana", Nationality = "Tanzaniana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 206, Name = "Timor-Leste", Citizenship = "Timorense", Nationality = "Timorense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 207, Name = "Togo", Citizenship = "Togolesa", Nationality = "Togolesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 208, Name = "Tonga", Citizenship = "Tonganesa", Nationality = "Tonganesa" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 209, Name = "Trinidad e Tobago", Citizenship = "Trinit�ria-tobagense", Nationality = "Trinit�ria-tobagense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 210, Name = "Tun�sia", Citizenship = "Tunisina", Nationality = "Tunisina" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 211, Name = "Turks e Caicos", Citizenship = "Turquense", Nationality = "Turquense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 212, Name = "Turquemenist�o", Citizenship = "Turquemena", Nationality = "Turquemena" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 213, Name = "Turquia", Citizenship = "Turca", Nationality = "Turca" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 214, Name = "Tuvalu", Citizenship = "Tuvaluana", Nationality = "Tuvaluana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 215, Name = "Ucr�nia", Citizenship = "Ucraniana", Nationality = "Ucraniana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 216, Name = "Uganda", Citizenship = "Ugandense", Nationality = "Ugandense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 217, Name = "Uruguai", Citizenship = "Uruguaia", Nationality = "Uruguaia" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 218, Name = "Uzbequist�o", Citizenship = "Usbeque", Nationality = "Usbeque" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 219, Name = "Vanuatu", Citizenship = "Vanuatuense", Nationality = "Vanuatuense" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 220, Name = "Vaticano", Citizenship = "Vaticana", Nationality = "Vaticana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 221, Name = "Venezuela", Citizenship = "Venezuelana", Nationality = "Venezuelana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 222, Name = "Vietn�", Citizenship = "Vietnamita", Nationality = "Vietnamita" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 223, Name = "Z�mbia", Citizenship = "Zambiana", Nationality = "Zambiana" });
            context.Countries.AddOrUpdate(new Data.Entities.Country() { Id = 224, Name = "Zimbabwe", Citizenship = "Zimbabuana", Nationality = "Zimbabuana" });
            #endregion

            #region Default Document Types
            context.DocumentTypes.AddOrUpdate(new Data.Entities.DocumentType() { Id = 1, Name = "Cart�o de Cidad�o" });
            context.DocumentTypes.AddOrUpdate(new Data.Entities.DocumentType() { Id = 2, Name = "Bilhete de Identidade" });
            context.DocumentTypes.AddOrUpdate(new Data.Entities.DocumentType() { Id = 3, Name = "Militar" });
            context.DocumentTypes.AddOrUpdate(new Data.Entities.DocumentType() { Id = 4, Name = "Passaporte" });
            context.DocumentTypes.AddOrUpdate(new Data.Entities.DocumentType() { Id = 5, Name = "Outro" });
            #endregion

            context.SaveChanges();
        }
    }
}
