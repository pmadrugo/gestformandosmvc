﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace gestformandosMVC.Data.DatabaseContexts
{
    public class GestFormandosDataContext : IdentityDbContext<ApplicationUser>
    {
        public GestFormandosDataContext() : base("name=GestFormandosConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<GestFormandosDataContext, gestformandosMVC.Migrations.Configuration>());
        }

        public static GestFormandosDataContext Create()
        {
            return new GestFormandosDataContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<Gender> Genders { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<DocumentType> DocumentTypes { get; set; }

        public DbSet<Professor> Professors { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Inscription> Inscriptions { get; set; }

        public DbSet<Ufcd> Ufcds { get; set; }

        public DbSet<Evaluation> Evaluations { get; set; }

        public DbSet<Schedule> Schedules { get; set; }

        public DbSet<HasClasses> HasClasses { get; set; }
    }
}