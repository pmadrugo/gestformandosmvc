﻿using gestformandosMVC.Data.Interfaces;
using gestformandosMVC.Helper;
using gestformandosMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Profile : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Primeiro Nome")]
        [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(40, ErrorMessage = "O {0} deverá conter entre {2} e {1} caractéres.")]
        public string FirstName { get; set; }

        [Display(Name = "Apelido")]
        [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(40, ErrorMessage = "O {0} deverá ter entre {2} e {1} caractéres.")]
        public string LastName { get; set; }

        [Display(Name = "Nome Completo")]
        [NotMapped]
        public virtual string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }

        [Display(Name = "Género")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher um {0}.")]
        [Required(ErrorMessage = "Tem que escolher um {0}.")]
        public int GenderId { get; set; }
        public virtual Gender Gender { get; set; }

        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "Tem que inserir uma {0}.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        [DateTimeOfAgeRange(ErrorMessage = "Tem de ser maior de idade.")]
        public DateTime Birthdate { get; set; }

        [Display(Name = "NIF")]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        [RegularExpression("^[1-35][0-9]{8,}$", ErrorMessage = "Por favor insira um {0} válido.")]
        public string VatNumber { get; set; }

        [Display(Name = "Local de Nascimento")]
        [Required(ErrorMessage = "O {0} deverá ter entre {2} e {1} caractéres.")]
        [StringLength(40, ErrorMessage = "Tem que inserir um {0}.")]
        public string Birthplace { get; set; }

        [Display(Name = "Nacionalidade")]
        [Required(ErrorMessage = "Tem que escolher uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher uma {0}.")]
        [ForeignKey("Nationality")]
        public int NationalityId { get; set; }
        public virtual Country Nationality { get; set; }

        [Display(Name = "Cidadania")]
        [Required(ErrorMessage = "Tem que escolher uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher uma {0}.")]
        [ForeignKey("Citizenship")]
        public int CitizenshipId { get; set; }
        public virtual Country Citizenship { get; set; }

        [Display(Name = "Tipo de Documento")]
        [Required(ErrorMessage = "Tem que escolher um {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher um {0}.")]
        public int DocumentTypeId { get; set; }
        public virtual DocumentType DocumentType { get; set; }

        [Display(Name = "Número do Documento")]
        [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [RegularExpression("^[0-9]{8,}$", ErrorMessage = "Por favor insira um {0} válido.")]
        [StringLength(30, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        public string DocumentNumber { get; set; }

        [Display(Name = "Data de Emissão")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Por favor escolha a {0} do documento.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DocumentEmissionDate { get; set; }

        [Display(Name = "Data de Validade")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Por favor escolha a {0} do documento.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DocumentExpirationDate { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Tem de inserir um {0}.")]
        public string Email { get; set; }

        public string ImagePath { get; set; }

        [NotMapped]
        [Display(Name = "Fotografia")]
        //[Required(ErrorMessage = "É necessário uma fotografia para validar o registo")]
        public HttpPostedFileBase ImageURL { get; set; }

        public virtual ICollection<Student> Student { get; set; }

        public virtual ICollection<Professor> Professor { get; set; }

        public virtual ICollection<Administration> Administration  { get; set; }

        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}