﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Course : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nome")] [Required(ErrorMessage = "O curso precisa de ter um {0}.")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "O {0} do curso tem de ter entre {2} e {1} caractéres.")]
        public string Name { get; set; }

        [Display(Name = "Referência")] [Required(ErrorMessage = "O curso precisa de ter uma {0}. (Veja os valores em http://www.catalogo.anqep.gov.pt/Qualificacoes)")]
        [StringLength(8, MinimumLength = 1, ErrorMessage = "A {0} consiste de um valor entre {2} e {1} caractéres.  (Veja os valores em http://www.catalogo.anqep.gov.pt/Qualificacoes)")] [Index(IsUnique = true)]
        [RegularExpression("^[0-9]+$", ErrorMessage = "A {0} é numérica. (Veja os valores em http://www.catalogo.anqep.gov.pt/Qualificacoes)")]
        public string Reference { get; set; }

        [Display(Name = "Descrição")] [StringLength(250, ErrorMessage = "A {0} não pode ter mais de {1} caractéres.")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public virtual ICollection<Team> Teams { get; set; }

        public virtual ICollection<Ufcd> Ufcds { get; set; }
    }
}