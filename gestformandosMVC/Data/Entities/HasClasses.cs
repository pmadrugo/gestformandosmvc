﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class HasClasses : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int TeamId { get; set; }
        public virtual Team Team { get; set; }

        public int ProfessorId { get; set; }
        public virtual Professor Professor { get; set; }

        public int UfcdId { get; set; }
        public virtual Ufcd Ufcd { get; set; }

        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}