﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Administration : IEntity
    {
        [Key]
        [Display(Name = "Número de Administração")]
        public int Id { get; set; }

        public int ProfileId { get; set; }
        public virtual Profile Profile { get; set; }
    }
}