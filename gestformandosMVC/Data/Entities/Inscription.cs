﻿using gestformandosMVC.Data.Interfaces;
using gestformandosMVC.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Inscription : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Data de Inscrição")] [Required(ErrorMessage = "Tem que escolher uma data de inscrição.")]
        //[DateTimeWithin6MonthsRange(ErrorMessage = "Só é permitida a inserção de datas de inscrição até 6 meses após o dia de hoje.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Display(Name = "Estudante")] [Required(ErrorMessage = "Tem que haver um {0} atribuido a esta inscrição.")]
        [Range(1, int.MaxValue, ErrorMessage = "Por favor escolha um {0}.")]
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        [Display(Name = "Turma")] [Required(ErrorMessage = "Tem que haver uma {0} atribuida a esta inscrição.")]
        [Range(1, int.MaxValue, ErrorMessage = "Por favor escolha uma {0}.")]
        public int TeamId { get; set; }
        public virtual Team Team { get; set; }

        [Display(Name = "Aceite?")]
        public bool Accepted { get; set; }
    }
}