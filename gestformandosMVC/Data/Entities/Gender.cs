﻿using gestformandosMVC.Data.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace gestformandosMVC.Data.Entities
{
    public class Gender : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Género")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} caractéres.")]
        public string Name { get; set; }

        public virtual ICollection<Profile> Profiles { get; set; }
    }
}