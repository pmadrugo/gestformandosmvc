﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Country : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "País")] [Required(ErrorMessage = "O {0} tem de ter um nome.")]
        [StringLength(40, ErrorMessage = "O {0} tem que conter entre {2} e {1} caractéres")]
        public string Name { get; set; }

        [Display(Name = "Nacionalidade")] [Required(ErrorMessage = "A {0} tem de ter um valor.")]
        [StringLength(40, ErrorMessage = "A {0} tem que conter entre {2} e {1} caractéres")]
        public string Nationality { get; set; }

        [Display(Name = "Cidadania")] [Required(ErrorMessage = "A {0} tem de ter um valor.")]
        [StringLength(40, ErrorMessage = "A {0} tem que conter entre {2} e {1} caractéres")]
        public string Citizenship { get; set; }

        [NotMapped]
        public virtual ICollection<Profile> Profiles { get; set; }
    }
}