﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Schedule : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "UFCD/Professor")]
        [Required(ErrorMessage = "É necessário ter uma UFCD e um professor associado.")]
        public int HasClassesId { get; set; }
        public virtual HasClasses HasClasses { get; set; }

        [Display(Name = "Data/Hora Início de Sessão")]
        [Required(ErrorMessage = "É necessário uma {0}.")]
        public DateTime BeginDateTime { get; set; }

        [Display(Name = "Data/Hora Fim de Sessão")]
        [Required(ErrorMessage = "É necessário uma {0}.")]
        public DateTime EndDateTime { get; set; }

        public bool IsProfessorPresent { get; set; }

        public virtual ICollection<Student> StudentsAttended { get; set; }
    }
}