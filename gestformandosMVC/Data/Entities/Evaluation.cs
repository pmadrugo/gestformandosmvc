﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Evaluation : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Estudante")] [Required(ErrorMessage = "A avaliação necessita de estar associada a um {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "A avaliação necessita de estar associada a um {0}.")]
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        [Display(Name = "UFCD")] [Required(ErrorMessage = "A avaliação necessita de estar associada a uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "A avaliação necessita de estar associada a uma {0}.")]
        public int UfcdId { get; set; }
        public virtual Ufcd Ufcd { get; set; }

        [Display(Name = "Avaliação")] [Required(ErrorMessage = "Tem de ter uma {0}.")]
        [Range(0, 20, ErrorMessage = "A {0} consiste de valores entre {1} e {2}.")]
        public double Grade { get; set; }

        [Display(Name = "Observações")]
        [StringLength(250, ErrorMessage = "As {0} só podem conter até {1} caractéres.")]
        public string Description { get; set; }
    }
}