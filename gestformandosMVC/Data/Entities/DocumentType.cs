﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class DocumentType : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Tipo de Documento")] [Required(ErrorMessage = "O {0} necessita de um nome.")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        public string Name { get; set; }

        public virtual ICollection<Profile> Profiles { get; set; }
    }
}