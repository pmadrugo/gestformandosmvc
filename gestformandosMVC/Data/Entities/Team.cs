﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Team : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nome")] [Required(ErrorMessage = "A turma necessita de ter um {0}.")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        public string Name { get; set; }

        [Display(Name = "Data de Início")] [DataType(DataType.Date)]
        [Required(ErrorMessage = "A turma tem de ter uma {0}.")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Data de Término")] [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        [Display(Name = "Capacidade")]
        [Required(ErrorMessage = "É necessário indicar quantos alunos vai ter a turma.")]
        [Range(1, int.MaxValue, ErrorMessage = "A turma tem de pelo menos ter um aluno!")]
        public int Capacity { get; set; }

        [Display(Name = "Inscrições Abertas")]
        public bool AllowSignins { get; set; }

        [Display(Name = "Curso")] [Required(ErrorMessage = "A turma tem de estar associada a um {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Por favor escolha um {0}.")]
        public int CourseId { get; set; }
        public virtual Course Course { get; set; }

        public virtual ICollection<Inscription> Inscriptions { get; set; }

        //[NotMapped]
        //public bool CanSign
        //{
        //    get
        //    {
        //        return (AllowSignins && (DateTime.Now.CompareTo(this.StartDate) < 0));
        //    }
        //}
    }
}