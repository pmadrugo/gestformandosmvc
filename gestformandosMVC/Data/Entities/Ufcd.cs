﻿using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Entities
{
    public class Ufcd : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "UFCD")] [Required(ErrorMessage = "A {0} precisa de um nome.")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "A {0} tem de ter entre {2} e {1} caractéres.")]
        public string Name { get; set; }

        [Display(Name = "Referência")] [Required(ErrorMessage = "É necessário uma {0}. (Veja os valores em http://www.catalogo.anqep.gov.pt/UFCD)")]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "A {0} tem deter entre {2} e {1} caractéres. (Veja os valores em http://www.catalogo.anqep.gov.pt/UFCD)")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "A {0} é numérica. (Veja os valores em http://www.catalogo.anqep.gov.pt/UFCD)")]
        public string Reference { get; set; }

        [Display(Name = "Descrição")] [StringLength(250, ErrorMessage = "A {0} tem de ter entre {2} e {1} caractéres.")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Display(Name = "Duração")] //[DataType(DataType.Time)]
        [Required(ErrorMessage = "É necessário preencher um valor para a {0}.")]
        [Range(0.1, double.MaxValue, ErrorMessage = "A {0} tem de ser superior a 0.")]
        //[DisplayFormat(DataFormatString = "{0:hh\\:mm", ApplyFormatInEditMode = true)]
        public double Duration { get; set; }

        [Display(Name = "Créditos")] [Required(ErrorMessage = "É necessário preencher o valor dos {0}.")]
        [Range(0, double.MaxValue, ErrorMessage = "Os {0} não podem ser um valor negativo.")]
        public double Credits { get; set; }

        public virtual ICollection<Evaluation> Evaluations { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public virtual ICollection<Professor> TaughtBy { get; set; }
    }
}