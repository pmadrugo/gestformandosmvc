﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IGenericRepository<T> where T: IEntity
    {
        Task<List<T>> GetListAsync();
        Task<bool> AddAsync(T entity);
        Task<bool> DeleteAsync(T entity);
        Task<bool> UpdateAsync(T entity);
        Task<T> GetByIdAsync(int Id);

        Task<bool> ExistsAsync(int Id);
        void Dispose();
    }
}
