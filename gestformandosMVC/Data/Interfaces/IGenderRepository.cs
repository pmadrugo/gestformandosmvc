﻿using System.Collections.Generic;
using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IGenderRepository : IGenericRepository<Gender>
    {
        Task<List<Gender>> GetListAsync();
    }
}