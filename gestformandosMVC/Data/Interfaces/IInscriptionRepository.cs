﻿using System.Collections.Generic;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IInscriptionRepository : IGenericRepository<Inscription>
    {
        List<Inscription> GetInscriptionsByStudent(int studentID);
        List<Inscription> GetPendingInscriptions();
    }
}