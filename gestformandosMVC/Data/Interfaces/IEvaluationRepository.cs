﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IEvaluationRepository : IGenericRepository<Evaluation>
    {
        Evaluation GetEvaluationByStudentAndUfcd(int studentID, int ufcdID);
    }
}