﻿using System.Collections.Generic;
using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface ITeamRepository : IGenericRepository<Team>
    {
        Task<List<Team>> GetSignableTeamsAsync();
        Task<List<Student>> GetTeamAllStudentsAsync(int teamID);
        Task<List<Team>> GetTeamsByStudentIdAsync(int studentId);
    }
}