﻿using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface ICourseRepository : IGenericRepository<Course>
    {
        Task<bool> AddWithUFCDsAsync(Course entity, string[] ufcdIds);
        Task<Course> GetCourseByIdWithUFCDsAsync(int Id);
        Task<bool> UpdateWithUFCDsAsync(Course entity, string[] ufcdIds);

    }
}