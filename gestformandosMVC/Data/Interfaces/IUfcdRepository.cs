﻿using System.Collections.Generic;
using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IUfcdRepository : IGenericRepository<Ufcd>
    {
        Task<List<Ufcd>> GetListAsync();
    }
}