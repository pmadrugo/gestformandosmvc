﻿using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IProfileRepository : IGenericRepository<Profile>
    {
        Task<Administration> AddProfileToAdministrationAsync(int profileId);
        Task<Professor> AddProfileToProfessorAsync(int profileId);
        Task<Student> AddProfileToStudentAsync(int profileId);
        Administration GetProfileAdministration(int profileId);
        Task<Administration> GetProfileAdministrationAsync(int profileId);
        Profile GetProfileByUserID(string userID);
        Task<Profile> GetProfileByUserIDAsync(string userID);
        Professor GetProfileProfessor(int profileId);
        Task<Professor> GetProfileProfessorAsync(int profileId);
        Student GetProfileStudent(int profileId);
        Task<Student> GetProfileStudentAsync(int profileId);
    }
}