﻿namespace gestformandosMVC.Data.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}