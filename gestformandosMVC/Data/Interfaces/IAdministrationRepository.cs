﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestformandosMVC.Data.Interfaces
{
    interface IAdministrationRepository : IGenericRepository<Administration>
    {
    }
}
