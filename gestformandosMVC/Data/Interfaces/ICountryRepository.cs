﻿using System.Collections.Generic;
using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;

namespace gestformandosMVC.Data.Interfaces
{
    public interface ICountryRepository : IGenericRepository<Country>
    {
        
    }
}