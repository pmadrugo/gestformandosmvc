﻿using System.Collections.Generic;
using System.Threading.Tasks;
using gestformandosMVC.Data.Entities;

namespace gestformandosMVC.Data.Interfaces
{
    public interface IDocumentTypeRepository : IGenericRepository<DocumentType>
    {
        Task<List<DocumentType>> GetListAsync();
    }
}