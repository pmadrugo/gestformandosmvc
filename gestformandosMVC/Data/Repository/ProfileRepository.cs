﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class ProfileRepository : GenericRepository<Profile>, IGenericRepository<Profile>, IProfileRepository
    {
        public ProfileRepository() { }

        #region Students
        public virtual Student GetProfileStudent(int profileId)
        {
            var result = _dbContext.Set<Profile>().FirstOrDefault(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Student.Count > 0)
                {
                    return result.Student.ElementAt(0);
                }
            }
            return null;
        }

        public virtual async Task<Student> GetProfileStudentAsync(int profileId)
        {
            var result = await _dbContext.Set<Profile>().FirstOrDefaultAsync(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Student.Count > 0)
                {
                    return result.Student.ElementAt(0);
                }
            }
            return null;
        }

        public virtual async Task<Student> AddProfileToStudentAsync(int profileId)
        {
            var result = await _dbContext.Set<Profile>().FirstOrDefaultAsync(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Student == null || result.Student.Count < 1)
                {
                    using (var stRepo = new StudentRepository())
                    {
                        var newEntry = new Student() { ProfileId = result.Id };

                        await stRepo.AddAsync(newEntry);
                        return newEntry;
                    }
                }
            }
            return null;
        }
        #endregion

        #region Professor
        public virtual Professor GetProfileProfessor(int profileId)
        {
            var result = _dbContext.Set<Profile>().FirstOrDefault(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Professor.Count > 0)
                {
                    return result.Professor.ElementAt(0);
                }
            }
            return null;
        }

        public virtual async Task<Professor> GetProfileProfessorAsync(int profileId)
        {
            var result = await _dbContext.Set<Profile>().FirstOrDefaultAsync(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Professor.Count > 0)
                {
                    return result.Professor.ElementAt(0);
                }
            }
            return null;
        }

        public virtual async Task<Professor> AddProfileToProfessorAsync(int profileId)
        {
            var result = await _dbContext.Set<Profile>().FirstOrDefaultAsync(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Professor == null || result.Professor.Count < 1)
                {
                    using (var profRepo = new ProfessorRepository())
                    {
                        var newEntry = new Professor() { ProfileId = result.Id };

                        await profRepo.AddAsync(newEntry);
                        return newEntry;
                    }
                }
            }
            return null;
        }
        #endregion

        #region Administration
        public virtual Administration GetProfileAdministration(int profileId)
        {
            var result = _dbContext.Set<Profile>().FirstOrDefault(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Administration.Count > 0)
                {
                    return result.Administration.ElementAt(0);
                }
            }
            return null;
        }
        public virtual async Task<Administration> GetProfileAdministrationAsync(int profileId)
        {
            var result = await _dbContext.Set<Profile>().FirstOrDefaultAsync(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Administration.Count > 0)
                {
                    return result.Administration.ElementAt(0);
                }
            }
            return null;
        }

        public virtual async Task<Administration> AddProfileToAdministrationAsync(int profileId)
        {
            var result = await _dbContext.Set<Profile>().FirstOrDefaultAsync(entity => entity.Id == profileId);
            if (result != null)
            {
                if (result.Administration == null || result.Administration.Count < 1)
                {
                    using (var profRepo = new AdministrationRepository())
                    {
                        var newEntry = new Administration() { ProfileId = result.Id };

                        await profRepo.AddAsync(newEntry);
                        return newEntry;
                    }
                }
            }
            return null;
        }
        #endregion

        public virtual async Task<Profile> GetProfileByUserIDAsync(string userID)
        {
            var result = await _dbContext.Set<Profile>()
                .Include(p => p.Student)
                .Include(p => p.Nationality)
                .Include(p => p.Citizenship)
                .Include(p => p.Professor)
                .Include(p => p.Administration)
                .FirstOrDefaultAsync(profile => profile.UserId == userID);
            return result;
        }

        public virtual Profile GetProfileByUserID(string userID)
        {
            return _dbContext.Set<Profile>()
                .Include(p => p.Student)
                .Include(p => p.Nationality)
                .Include(p => p.Citizenship)
                .Include(p => p.Professor)
                .Include(p => p.Administration)
                .FirstOrDefault(profile => profile.UserId == userID);
        }

        public virtual async Task<List<Profile>> GetProfileOfProfessorsAsync()
        {
            return await _dbContext.Set<Professor>()
                .Include(p => p.Profile)
                .Select(p => p.Profile).ToListAsync();
        }

        public virtual async Task<List<Profile>> GetProfileOfStudentsAsync()
        {
            return await _dbContext.Set<Student>()
                .Include(s => s.Profile)
                .Select(s => s.Profile).ToListAsync();
        }

        public virtual async Task<List<Profile>> GetProfileOfAdminAsync()
        {
            return await _dbContext.Set<Administration>()
                .Include(a => a.Profile)
                .Select(a => a.Profile).ToListAsync();
        }
    }
}