﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {

    }
}