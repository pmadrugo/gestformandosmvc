﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class InscriptionRepository : GenericRepository<Inscription>, IGenericRepository<Inscription>, IInscriptionRepository
    {
        public InscriptionRepository() { }

        public List<Inscription> GetInscriptionsByStudent(int studentID)
        {
            var result = (from inscriptions in _dbContext.Inscriptions
                          where inscriptions.StudentId == studentID
                          select inscriptions).ToList();

            if (result != null)
            {
                return result.OrderByDescending(ins => ins.Id).ToList();
            }

            return null;
        }

        public List<Inscription> GetPendingInscriptions()
        {

            var result = (from inscriptions in _dbContext.Inscriptions
                          join team in _dbContext.Teams on inscriptions.TeamId equals team.Id
                          where
                            (
                                inscriptions.Accepted == false &&
                                team.AllowSignins == true
                             )
                          select inscriptions).ToList();

            if (result != null)
            {
                return result.OrderByDescending(ins => ins.Id).ToList();
            }
            return null;
        }
    }
}