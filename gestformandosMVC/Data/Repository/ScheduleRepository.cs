﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class ScheduleRepository : GenericRepository<Schedule>, IGenericRepository<Schedule>
    {
        public List<Schedule> GetScheduleOfProfessor(int professorId)
        {
            return _dbContext.Set<Schedule>()
                .Where(x => x.HasClasses.ProfessorId == professorId)
                .ToList();
        }

        public List<Schedule> GetScheduleOfStudent(int studentId)
        {
            return _dbContext.Set<Schedule>()
                .Where(x => 
                    x.HasClasses.Team.Inscriptions.Any(y => y.StudentId == studentId && y.Accepted))
                .ToList();
        }

        public List<Schedule> GetScheduleOfTeam(int teamID)
        {
            return _dbContext.Set<Schedule>()
                .Where(x => x.HasClasses.TeamId == teamID).ToList();
        }
    }
}