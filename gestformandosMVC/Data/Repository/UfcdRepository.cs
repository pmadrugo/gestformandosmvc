﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class UfcdRepository : GenericRepository<Ufcd>, IGenericRepository<Ufcd>, IUfcdRepository
    {
        public UfcdRepository() { }

        public override async Task<List<Ufcd>> GetListAsync()
        {
            var result = await base.GetListAsync();
            if (result != null)
            {
                return result.OrderBy(ufcd => ufcd.Name).ToList();
            }
            return null;
        }
    }
}