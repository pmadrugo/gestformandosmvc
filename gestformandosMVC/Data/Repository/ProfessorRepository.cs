﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class ProfessorRepository : GenericRepository<Professor>, IProfessorRepository
    {
        public Professor GetById(int Id)
        {
            return _dbContext.Professors
                .Include(x => x.Profile)
                .Where(x => x.Id == Id).FirstOrDefault();
        }

        public async Task<bool> UpdateWithUFCDsAsync(Professor entity, string[] ufcdIds)
        {
            var existingItem = _dbContext.Entry<Professor>(entity);
            existingItem.State = EntityState.Modified;

            await existingItem.Collection(item => item.UFCDs).LoadAsync();

            entity.UFCDs.Clear();
            foreach (var ufcdId in ufcdIds)
            {
                var id = int.Parse(ufcdId);
                var ufcd = await _dbContext.Ufcds.FirstOrDefaultAsync(i => i.Id == id);
                if (ufcd != null)
                {
                    entity.UFCDs.Add(ufcd);
                }

            }

            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}