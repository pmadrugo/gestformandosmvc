﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;

namespace gestformandosMVC.Data.Repository
{
    public class CourseRepository : GenericRepository<Course>, IGenericRepository<Course>, ICourseRepository
    {
        public CourseRepository() { }

        public async Task<Course> GetCourseByIdWithUFCDsAsync(int Id)
        {
            return await this._dbContext
                .Courses.Include(course => course.Ufcds)
                .FirstOrDefaultAsync(course => course.Id == Id);
        }

        public async Task<bool> AddWithUFCDsAsync(Course entity, string[] ufcdIds)
        {
            try
            {
                entity.Ufcds = new List<Ufcd>();
                foreach (var ufcdId in ufcdIds)
                {
                    var id = int.Parse(ufcdId);
                    var ufcd = await _dbContext.Ufcds.FirstOrDefaultAsync(i => i.Id == id);
                    if (ufcd != null)
                    {
                        entity.Ufcds.Add(ufcd);
                    }
                }

                _dbContext.Set<Course>().Add(entity);
                return await SaveAllAsync();
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> UpdateWithUFCDsAsync(Course entity, string[] ufcdIds)
        {
            var existingItem = _dbContext.Entry<Course>(entity);
            existingItem.State = EntityState.Modified;

            await existingItem.Collection(item => item.Ufcds).LoadAsync();

            entity.Ufcds.Clear();
            foreach (var ufcdId in ufcdIds)
            {
                var id = int.Parse(ufcdId);
                var ufcd = await _dbContext.Ufcds.FirstOrDefaultAsync(i => i.Id == id);
                if (ufcd != null)
                {
                    entity.Ufcds.Add(ufcd);
                }

            }

            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}