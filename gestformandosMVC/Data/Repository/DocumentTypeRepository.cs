﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class DocumentTypeRepository : GenericRepository<DocumentType>, IGenericRepository<DocumentType>, IDocumentTypeRepository
    {
        public DocumentTypeRepository() { }

        public override async Task<List<DocumentType>> GetListAsync()
        {
            var result = await base.GetListAsync();
            if (result != null)
            {
                return result.OrderBy(docType => docType.Name).ToList();
            }
            return result;
        }
    }
}