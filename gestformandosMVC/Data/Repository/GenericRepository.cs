﻿using System.Collections.Generic;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Configuration;
using System;

namespace gestformandosMVC.Data.Repository
{
    public class GenericRepository<T> : IDisposable, IGenericRepository<T> where T : class, IEntity
    {
        protected GestFormandosDataContext _dbContext;

        public GenericRepository()
        {
            this._dbContext = new GestFormandosDataContext();
        }

        public virtual async Task<List<T>> GetListAsync()
        { 
            return await _dbContext.Set<T>().ToListAsync();
        }

        public virtual async Task<bool> AddAsync(T entity)
        {
            try
            {
                _dbContext.Set<T>().Add(entity);
                return await SaveAllAsync();
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        public virtual async Task<bool> DeleteAsync(T entity)
        {
            try
            {
                _dbContext.Set<T>().Remove(entity);
                return await SaveAllAsync();
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public virtual async Task<T> GetByIdAsync(int Id)
        {
            var result = await _dbContext.Set<T>().FirstOrDefaultAsync(entity => entity.Id == Id);

            return result;
        }

        public virtual async Task<bool> UpdateAsync(T entity)
        {
            var existingValue = await _dbContext
                .Set<T>()
                .FirstOrDefaultAsync(row => row.Id == entity.Id);

            if (existingValue != null)
            {
                try
                {
                    _dbContext.Entry<T>(existingValue).CurrentValues.SetValues(entity);
                    _dbContext.Entry<T>(existingValue).State = EntityState.Modified;
                    return await SaveAllAsync();
                }
                catch (Exception ex)
                {

                }
            }
            return false;
        }

        protected virtual async Task<bool> SaveAllAsync()
        {
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public virtual async Task<bool> ExistsAsync(int Id)
        {
            return await _dbContext.Set<T>().AnyAsync(entity => entity.Id == Id);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}