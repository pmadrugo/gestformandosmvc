﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class GenderRepository : GenericRepository<Gender>, IGenericRepository<Gender>, IGenderRepository
    {
        public GenderRepository() { }

        public override async Task<List<Gender>> GetListAsync()
        {
            var result = await base.GetListAsync();
            if (result != null)
            {
                return result.OrderBy(gender => gender.Name).ToList();
            }
            return result;
        }
    }
}