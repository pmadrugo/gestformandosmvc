﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class EvaluationRepository : GenericRepository<Evaluation>, IGenericRepository<Evaluation>, IEvaluationRepository
    {
        public EvaluationRepository()
        {

        }

        public Evaluation GetEvaluationByStudentAndUfcd(int studentID, int ufcdID)
        {
            return _dbContext.Set<Evaluation>()
                .Where(x => x.StudentId == studentID && x.UfcdId == ufcdID)
                .FirstOrDefault();
        }

        public List<Evaluation> GetEvaluationsByStudent(int studentID)
        {
            return _dbContext.Set<Evaluation>()
                .Where(x => x.StudentId == studentID)
                .ToList();
        }
    }
}