﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class HasClassesRepository : GenericRepository<HasClasses>, IGenericRepository<HasClasses>
    {

        public List<HasClasses> GetClassesByTeamId(int teamId)
        {
            return _dbContext.Set<HasClasses>()
                .Where(x => x.TeamId == teamId).ToList();
        }

        public HasClasses GetByTeamIdAndUfcdId(int teamId, int ufcdId)
        {
            return _dbContext.Set<HasClasses>()
                .Where(x => x.TeamId == teamId && x.UfcdId == ufcdId).FirstOrDefault();
        }
    }
}