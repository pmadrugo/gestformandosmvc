﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;

namespace gestformandosMVC.Data.Repository
{
    public class TeamRepository : GenericRepository<Team>, IGenericRepository<Team>, ITeamRepository
    {
        public TeamRepository() { }

        public async Task<List<Team>> GetSignableTeamsAsync()
        {
            // AllowSignins && (DateTime.Now.CompareTo(this.StartDate) < 0
            return await _dbContext.Teams
                .Where(x => x.AllowSignins == true && x.StartDate <= DateTime.Now)
                .OrderByDescending(x => x.Id)
                .ToListAsync();
        }

        public async Task<List<Team>> GetTeamsByStudentIdAsync(int studentId)
        {
            return await _dbContext.Teams
                .Include(x => x.Inscriptions)
                .Where(x => x.Inscriptions.Any(y => y.StudentId == studentId && y.Accepted == true))
                .OrderBy(x => x.Id)
                .ToListAsync(); ;
        }

        public async Task<List<Student>> GetTeamAllStudentsAsync(int teamID)
        {
            return await _dbContext.Students
                .Include(x => x.Inscriptions)
                .Where(x => x.Inscriptions.Any(y => y.Accepted == true && y.TeamId == teamID))
                .OrderByDescending(x => x.Id)
                .ToListAsync();
        }
    }
}