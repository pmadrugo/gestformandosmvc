﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace gestformandosMVC.Data.Repository
{
    public class CountryRepository : GenericRepository<Country>, IGenericRepository<Country>, ICountryRepository
    {
        public CountryRepository() { }

        public override async Task<List<Country>> GetListAsync()
        {
            var result = await base.GetListAsync();
            if (result != null)
            {
                return result.OrderBy(country => country.Name).ToList();
            }
            return result;
        }
    }
}