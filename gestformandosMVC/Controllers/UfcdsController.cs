﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;

namespace gestformandosMVC.Controllers
{
    public class UfcdsController : Controller
    {
        private UfcdRepository ufcdRepo = new UfcdRepository();

        // GET: Ufcds
        public async Task<ActionResult> Index()
        {
            return View((await ufcdRepo.GetListAsync()).OrderBy(x => x.Reference));
        }

        // GET: Ufcds/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ufcd ufcd = await ufcdRepo.GetByIdAsync(id.Value);
            if (ufcd == null)
            {
                return HttpNotFound();
            }
            return View(ufcd);
        }

        // GET: Ufcds/Create
        [Authorize(Roles = "Administrativo,SuperUser,Director")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Ufcds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Administrativo,SuperUser,Director")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Reference,Description,Duration,Credits")] Ufcd ufcd)
        {
            if (ModelState.IsValid)
            {
                await ufcdRepo.AddAsync(ufcd);
                return RedirectToAction("Index");
            }

            return View(ufcd);
        }

        // GET: Ufcds/Edit/5
        [Authorize(Roles = "Administrativo,SuperUser,Director")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ufcd ufcd = await ufcdRepo.GetByIdAsync(id.Value);
            if (ufcd == null)
            {
                return HttpNotFound();
            }
            return View(ufcd);
        }

        // POST: Ufcds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Administrativo,SuperUser,Director")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Reference,Description,Duration,Credits")] Ufcd ufcd)
        {
            if (ModelState.IsValid)
            {
                await ufcdRepo.UpdateAsync(ufcd);
                return RedirectToAction("Index");
            }
            return View(ufcd);
        }

        // GET: Ufcds/Delete/5
        //public async Task<ActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Ufcd ufcd = await ufcdRepo.GetByIdAsync(id.Value);
        //    if (ufcd == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(ufcd);
        //}

        // POST: Ufcds/Delete/5
        [Authorize(Roles = "Administrativo,SuperUser,Director")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Ufcd ufcd = await ufcdRepo.GetByIdAsync(id);
            await ufcdRepo.DeleteAsync(ufcd);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ufcdRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
