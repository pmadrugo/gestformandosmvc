﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.ViewModels;
using Microsoft.AspNet.Identity;
using gestformandosMVC.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace gestformandosMVC.Controllers
{
    [Authorize]
    public class ProfilesController : Controller
    {
        private ProfileRepository profileRepo = new ProfileRepository();

        // GET: Profiles
        public async Task<ActionResult> Index()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Profiles/{id}
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var profile = await profileRepo.GetByIdAsync(id.Value);
            if (profile != null)
            {
                ProfileViewModel profileViewModel;

                if (profile != await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId()))
                {
                    if (!User.IsInRole("Administrativo") &&
                    !User.IsInRole("Director") &&
                    !User.IsInRole("SuperUser"))
                    {
                        return RedirectToAction("Me");
                    }

                    profileViewModel = new ProfileViewModel()
                    {
                        Profile = profile,
                        IsSelf = false,
                        ProfessorActive = false,
                        StudentActive = false,
                        AdministrationActive = false
                    };
                }
                else
                {
                    profileViewModel = new ProfileViewModel()
                    {
                        Profile = profile,
                        IsSelf = true,
                        ProfessorActive = false,
                        StudentActive = false,
                        AdministrationActive = false
                    };
                }

                using (GestFormandosDataContext userAuth = new GestFormandosDataContext())
                {
                    var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userAuth));

                    var activeRoles = userManager.GetRoles(profile.UserId);
                    if (activeRoles.Contains("Administrativo"))
                    {
                        profileViewModel.AdministrationActive = true;
                    }
                    if (activeRoles.Contains("Formador"))
                    {
                        profileViewModel.ProfessorActive = true;
                    }
                    if (activeRoles.Contains("Estudante"))
                    {
                        profileViewModel.StudentActive = true;
                    }

                    return View(profileViewModel);
                }
                    
            }

            return HttpNotFound();
        }

        // GET: Profiles/Me
        public async Task<ActionResult> Me()
        {
            if (User.IsInRole("SuperUser"))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
            if (profile != null)
            {
                var profileViewModel = new ProfileViewModel()
                {
                    Profile = profile,
                    IsSelf = true
                };

                return View("Details", profileViewModel);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Profiles/Teaching
        public async Task<ActionResult> Teaching(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (var professorRepo = new ProfessorRepository())
            {
                var professor = professorRepo.GetById(id.Value);
                if (professor != null)
                {
                    var viewModel = new ProfessorTeachesViewModel()
                    {
                        FullName = professor.Profile.FullName,
                        Professor = professor,
                        ProfessorCurrentUFCDs = professor.UFCDs.ToList()
                    };

                    return View(viewModel);
                }

                return HttpNotFound();
            }
        }

        // GET: Profiles/Teaches
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Teaches(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var professorRepo = new ProfessorRepository())
            {
                var professor = professorRepo.GetById(id.Value);
                if (professor != null)
                {
                    var viewModel = new ProfessorTeachesViewModel()
                    {
                        FullName = professor.Profile.FullName,
                        Professor = professor,
                        ProfessorCurrentUFCDs = professor.UFCDs.ToList()
                    };

                    using (var ufcdRepo = new UfcdRepository())
                    {
                        ViewBag.UFCDList = await ufcdRepo.GetListAsync();

                        return View(viewModel);
                    }
                }

                return HttpNotFound();
            }
        }

        // POST: Profiles/Teaches
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Teaches(int id)
        {
            using (var professorRepo = new ProfessorRepository())
            {
                var professor = await professorRepo.GetByIdAsync(id);
                if (professor != null)
                {
                    var ufcds = Request.Form["Ufcds"];
                    if (ufcds != null)
                    {
                        var ufcdIds = ufcds.Split(',');

                        if (await professorRepo.UpdateWithUFCDsAsync(professor, ufcdIds))
                        {
                            ViewBag.SuccessMessage = "UFCDs leccionadas alteradas com sucesso!";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "UFCDs leccionadas alteradas sem sucesso!";
                        }
                    }
                    else
                    {
                        if (await professorRepo.UpdateWithUFCDsAsync(professor, new string[] { }))
                        {
                            ViewBag.SuccessMessage = "UFCDs leccionadas alteradas com sucesso!";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "UFCDs leccionadas alteradas sem sucesso!";
                        }
                    }

                    var viewModel = new ProfessorTeachesViewModel()
                    {
                        FullName = professor.Profile.FullName,
                        Professor = professor,
                        ProfessorCurrentUFCDs = professor.UFCDs.ToList()
                    };

                    using (var ufcdRepo = new UfcdRepository())
                    {
                        ViewBag.UFCDList = await ufcdRepo.GetListAsync();

                        return View(viewModel);
                    }
                }

                return HttpNotFound();
            }
        }

        // GET: Professors
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Professors()
        {
            return View(await profileRepo.GetProfileOfProfessorsAsync());
        }

        // GET: Students
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Students()
        {
            return View(await profileRepo.GetProfileOfStudentsAsync());
        }

        // GET: Administration
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Admin()
        {
            return View(await profileRepo.GetProfileOfAdminAsync());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                profileRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
