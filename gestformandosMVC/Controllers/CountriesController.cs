﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Data.Interfaces;

namespace gestformandosMVC.Controllers
{
    [Authorize(Roles = "SuperUser")]
    public class CountriesController : Controller
    {
        private CountryRepository countryRepo = new CountryRepository();

        // GET: Countries
        public async Task<ActionResult> Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            return View(await countryRepo.GetListAsync());
        }

        // GET: Countries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Nationality,Citizenship")] Country country)
        {
            if (ModelState.IsValid)
            {
                await countryRepo.AddAsync(country);
                return RedirectToAction("Index");
            }

            return View(country);
        }

        // GET: Countries/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Country country = await countryRepo.GetByIdAsync(id.Value);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // POST: Countries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Nationality,Citizenship")] Country country)
        {
            if (ModelState.IsValid)
            {
                await countryRepo.UpdateAsync(country);
                return RedirectToAction("Index");
            }
            return View(country);
        }

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Country country = await countryRepo.GetByIdAsync(id);
            if(!(await countryRepo.DeleteAsync(country)))
            {
                TempData["ErrorMessage"] = $"Este registo de momento não pode ser apagado, certifique-se que o mesmo não possui outros registos associados.";
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                countryRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
