﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Helpers;
using gestformandosMVC.Data.Interfaces;

namespace gestformandosMVC.Controllers
{
    [Authorize(Roles = "Administrativo, Director, SuperUser")]
    public class CoursesController : Controller
    {
        private CourseRepository courseRepo = new CourseRepository();

        // GET: Courses
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            return View(await courseRepo.GetListAsync());
        }

        // GET: Courses/Details/5
        [AllowAnonymous]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = await courseRepo.GetCourseByIdWithUFCDsAsync(id.Value);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // GET: Courses/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.UFCDList = (await DropDownHelper.GetUfcdsAsync()).OrderBy(ufcd => ufcd.Reference);

            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Reference,Description")] Course course)
        {
            if (ModelState.IsValid)
            {
                var ufcds = Request.Form["Ufcds"];
                if (ufcds != null)
                {
                    var ufcdIds = ufcds.Split(',');

                    await courseRepo.AddWithUFCDsAsync(course, ufcdIds);
                }
                else
                {
                    await courseRepo.AddAsync(course);
                }
                return RedirectToAction("Index");
            }

            ViewBag.UFCDList = (await DropDownHelper.GetUfcdsAsync()).OrderBy(ufcd => ufcd.Reference);

            return View(course);
        }

        // GET: Courses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = await courseRepo.GetCourseByIdWithUFCDsAsync(id.Value);
            if (course == null)
            {
                return HttpNotFound();
            }

            ViewBag.UFCDList = (await DropDownHelper.GetUfcdsAsync()).OrderBy(ufcd => ufcd.Reference);

            return View(course);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Reference,Description")] Course course)
        {
            if (ModelState.IsValid)
            {
                var ufcds = Request.Form["Ufcds"];
                if (ufcds != null)
                {
                    var ufcdIds = ufcds.Split(',');

                    await courseRepo.UpdateWithUFCDsAsync(course, ufcdIds);
                }
                else
                {
                    await courseRepo.UpdateAsync(course);
                }
                
                return RedirectToAction("Index");
            }

            course.Ufcds = (await courseRepo.GetByIdAsync(course.Id)).Ufcds;
            ViewBag.UFCDList = (await DropDownHelper.GetUfcdsAsync()).OrderBy(ufcd => ufcd.Reference);

            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = await courseRepo.GetByIdAsync(id.Value);
            if (course == null)
            {
                return HttpNotFound();
            }
            if (!(await courseRepo.DeleteAsync(course)))
            {
                TempData["ErrorMessage"] = $"Este registo de momento não pode ser apagado, certifique-se que o mesmo não possui outros registos associados.";
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                courseRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
