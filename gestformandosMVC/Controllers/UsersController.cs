﻿using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Helpers;
using gestformandosMVC.Models;
using gestformandosMVC.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace gestformandosMVC.Controllers
{
    public class UsersController : Controller
    {
        private GestFormandosDataContext db = new GestFormandosDataContext();

        // GET: /Users/
        [Authorize(Roles = "SuperUser, Director, Administrativo")]
        public async Task<ActionResult> Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var users = userManager.Users.ToList();
            
            var userViews = new List<UserViewModel>();

            using (var profileRepo = new ProfileRepository())
            {
                foreach (var user in users.Where(x => !userManager.GetRoles(x.Id).Contains("SuperUser")))
                {
                    var viewUser = new UserViewModel()
                    {
                        EMail = user.Email,
                        Username = user.UserName,
                        UserID = user.Id
                    };
                    var profile = await profileRepo.GetProfileByUserIDAsync(user.Id);
                    if (profile != null)
                    {
                        viewUser.FirstName = profile.FirstName;
                        viewUser.LastName = profile.LastName;
                    }

                    viewUser.Roles = new List<RoleViewModel>();
                    foreach (var role in await userManager.GetRolesAsync(viewUser.UserID))
                    {
                        viewUser.Roles.Add(new RoleViewModel()
                        {
                            Name = role
                        });
                    }

                    userViews.Add(viewUser);
                }
            }
                

            return View(userViews.OrderBy(user => user.Username));
        }

        // GET: /Users/Roles
        [Authorize(Roles = "SuperUser, Director, Administrativo")]
        public ActionResult Roles (string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();
            var users = userManager.Users.ToList();

            var user = users.Find(u => u.Id == userID);
            if (user == null)
            {
                return HttpNotFound();
            }

            var rolesView = new List<RoleViewModel>();
            foreach (var item in user.Roles)
            {
                var role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleViewModel()
                {
                    Name = role.Name,
                    RoleID = role.Id
                };

                rolesView.Add(roleView);
            }

            var userView = new UserViewModel()
            {
                EMail = user.Email,
                Username = user.UserName,
                UserID = user.Id,
                Roles = rolesView
            };

            return View(userView);
        }

        // GET: /Users/AddRole
        [Authorize(Roles = "SuperUser, Director, Administrativo")]
        public async Task<ActionResult> AddRole(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userView = new UserViewModel()
            {
                EMail = user.Email,
                Username = user.UserName,
                UserID = user.Id
            };

            var userRoles = await userManager.GetRolesAsync(user.Id);

            var rolesList = DropDownHelper.GetRolesNewAccount().Where(x => !userRoles.Contains(x.Name)).ToList();

            ViewBag.RoleID = new SelectList(rolesList, "Id", "Name");

            return View(userView);
        }

        // POST: /Users/AddRole
        [HttpPost]
        [Authorize(Roles = "SuperUser, Director, Administrativo")]
        public async Task<ActionResult> AddRole(string userID, FormCollection form)
        {
            var roleID = Request["RoleID"];

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = userManager.Users.ToList();
            var user = users.Find(u => u.Id == userID);

            var userView = new UserViewModel()
            {
                EMail = user.Email,
                Username = user.UserName,
                UserID = user.Id
            };

            if (string.IsNullOrEmpty(roleID))
            {
                ViewBag.Error = "Tem que seleccionar um cargo";
                ViewBag.RoleID = new SelectList(DropDownHelper.GetRoles(), "Id", "Name");

                return View(userView);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();
            var role = roles.Find(r => r.Id == roleID);

            if ((await userManager.IsInRoleAsync(userID, role.Name) == false))
            {
                await userManager.AddToRoleAsync(userID, role.Name);
                using (var profileRepo = new ProfileRepository())
                {
                    switch (role.Name)
                    {
                        case "Administrativo":
                        {
                            var admin = await profileRepo.AddProfileToAdministrationAsync((await profileRepo.GetProfileByUserIDAsync(userID)).Id);
                        }
                        break;
                        case "Formador":
                        {
                            var prof = await profileRepo.AddProfileToProfessorAsync((await profileRepo.GetProfileByUserIDAsync(userID)).Id);
                        }
                        break;
                        case "Estudante":
                        {
                            var student = await profileRepo.AddProfileToStudentAsync((await profileRepo.GetProfileByUserIDAsync(userID)).Id);
                        }
                        break;
                    }

                    await userManager.UpdateAsync(user);
                }  
            }

            var rolesView = new List<RoleViewModel>();
            foreach ( var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleViewModel()
                {
                    Name = role.Name,
                    RoleID = role.Id
                };

                rolesView.Add(roleView);
            }

            userView = new UserViewModel()
            {
                EMail = user.Email,
                Username = user.UserName,
                UserID = user.Id,
                Roles = rolesView
            };

            return View("Roles", userView);
        }

        // POST: /Users/RemoveRole
        [Authorize(Roles = "Director,SuperUser")]
        [HttpPost]
        public async Task<ActionResult> RemoveRole(string userID, string roleID)
        {
            if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(roleID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var users = userManager.Users.ToList();
            var roles = roleManager.Roles.ToList();

            var role = roles.Find(r => r.Id == roleID);
            var user = users.Find(u => u.Id == userID);

            if (!await userManager.IsInRoleAsync(user.Id, "SuperUser"))
            {
                if (await userManager.IsInRoleAsync(user.Id, role.Name))
                {
                    await userManager.RemoveFromRoleAsync(user.Id, role.Name);
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Não pode ser retirada permissões ao SuperUser";
            }

            var rolesView = new List<RoleViewModel>();
            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);
                var roleView = new RoleViewModel()
                {
                    Name = role.Name,
                    RoleID = role.Id
                };

                rolesView.Add(roleView);
            }

            var userView = new UserViewModel()
            {
                Username = user.UserName,
                EMail = user.Email,
                Roles = rolesView,
                UserID = user.Id
            };

            return View("Roles", userView);
        }

    }
}