﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Data.Interfaces;

namespace gestformandosMVC.Controllers
{
    [Authorize(Roles = "SuperUser")]
    public class DocumentTypesController : Controller
    {
        private DocumentTypeRepository docTypeRepo = new DocumentTypeRepository();
        // GET: DocumentTypes
        public async Task<ActionResult> Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            return View(await docTypeRepo.GetListAsync());
        }

        // GET: DocumentTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DocumentTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] DocumentType documentType)
        {
            if (ModelState.IsValid)
            {
                await docTypeRepo.AddAsync(documentType);
                return RedirectToAction("Index");
            }

            return View(documentType);
        }

        // GET: DocumentTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentType documentType = await docTypeRepo.GetByIdAsync(id.Value);
            if (documentType == null)
            {
                return HttpNotFound();
            }
            return View(documentType);
        }

        // POST: DocumentTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] DocumentType documentType)
        {
            if (ModelState.IsValid)
            {
                await docTypeRepo.UpdateAsync(documentType);
                return RedirectToAction("Index");
            }
            return View(documentType);
        }

        // POST: DocumentTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DocumentType documentType = await docTypeRepo.GetByIdAsync(id);
            if (!(await docTypeRepo.DeleteAsync(documentType)))
            {
                TempData["ErrorMessage"] = $"Este registo de momento não pode ser apagado, certifique-se que o mesmo não possui outros registos associados.";
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                docTypeRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
