﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using Syncfusion.EJ2;
using gestformandosMVC.ViewModels.Schedules;
using gestformandosMVC.ViewModels.Units;
using gestformandosMVC.Extensions;
using Microsoft.AspNet.Identity;

namespace gestformandosMVC.Controllers
{
    [Authorize]
    public class SchedulesController : Controller
    {
        private ProfileRepository profileRepo = new ProfileRepository();
        private ScheduleRepository schedRepo = new ScheduleRepository();
        private TeamRepository teamRepo = new TeamRepository();
        private HasClassesRepository hasClassRepo = new HasClassesRepository();

        // GET: Schedules/Me
        [Authorize(Roles = "Estudante")]
        public async Task<ActionResult> Me()
        {
            var currentStudent = await profileRepo.GetProfileStudentAsync((await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId())).Id);

            return RedirectToAction("Student", new { id = currentStudent.Id });
        }

        // GET: Schedules/Team/{id}
        public async Task<ActionResult> Team(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View("ScheduleProfessor", id.Value);
        }

        // GET: Schedules/Student/{id}
        public async Task<ActionResult> Student(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View("ScheduleStudent", id.Value);
        }

        // POST: Schedules/Student/{id}
        [HttpPost]
        public async Task<JsonResult> Student(int id)
        {
            var studentSchedule = schedRepo.GetScheduleOfStudent(id);
            List<ScheduleViewModel> scheduleVM = new List<ScheduleViewModel>();
            foreach (var schedule in studentSchedule)
            {
                var studentsAttended = schedule.StudentsAttended.Select(x =>
                    new ScheduleProfileUnitViewModel()
                    {
                        FirstName = x.Profile.FirstName,
                        LastName = x.Profile.LastName,
                        StudentId = x.Profile.Student.ElementAt(0).Id,
                        ProfileId = x.Profile.Id
                    }).ToList();

                var studentsList = (await teamRepo.GetTeamAllStudentsAsync(schedule.HasClasses.TeamId)).Select(x =>
                    new ScheduleProfileUnitViewModel()
                    {
                        FirstName = x.Profile.FirstName,
                        LastName = x.Profile.LastName,
                        StudentId = x.Profile.Student.ElementAt(0).Id,
                        ProfileId = x.Profile.Id,
                        Present = studentsAttended.Any(y => y.ProfileId == x.ProfileId)
                    }).ToList();

                var sched = new ScheduleViewModel()
                {
                    Title = $"{schedule.HasClasses.Ufcd.Reference} - {schedule.HasClasses.Ufcd.Name}",
                    Id = schedule.Id.ToString(),
                    BeginDateTime = schedule.BeginDateTime,
                    EndDateTime = schedule.EndDateTime,
                    HasClassesId = schedule.HasClasses.Id.ToString(),
                    IsProfessorPresent = schedule.IsProfessorPresent,
                    Students = studentsList,
                    Team = schedule.HasClasses.Team.Name,
                    Ufcd = $"{schedule.HasClasses.Ufcd.Reference} - {schedule.HasClasses.Ufcd.Name}"
                };
                scheduleVM.Add(sched);
            }

            return new JsonNetResult(scheduleVM);
        }

        // POST: Schedules/Team/{id}
        [HttpPost]
        [Authorize(Roles = "Administrativo,SuperUser, Director, Formador")]
        public async Task<ActionResult> Team(int id)
        {
            var studentSchedule = schedRepo.GetScheduleOfTeam(id);
            List<ScheduleViewModel> scheduleVM = new List<ScheduleViewModel>();
            foreach (var schedule in studentSchedule)
            {
                var studentsAttended = schedule.StudentsAttended.Select(x =>
                    new ScheduleProfileUnitViewModel()
                    {
                        FirstName = x.Profile.FirstName,
                        LastName = x.Profile.LastName,
                        StudentId = x.Profile.Student.ElementAt(0).Id,
                        ProfileId = x.Profile.Id
                    }).ToList();

                var studentsList = (await teamRepo.GetTeamAllStudentsAsync(schedule.HasClasses.TeamId)).Select(x =>
                    new ScheduleProfileUnitViewModel()
                    {
                        FirstName = x.Profile.FirstName,
                        LastName = x.Profile.LastName,
                        StudentId = x.Profile.Student.ElementAt(0).Id,
                        ProfileId = x.Profile.Id,
                        Present = studentsAttended.Any(y => y.ProfileId == x.ProfileId)
                    }).ToList();

                var sched = new ScheduleViewModel()
                {
                    Title = $"{schedule.HasClasses.Ufcd.Reference} - {schedule.HasClasses.Ufcd.Name}",
                    Id = schedule.Id.ToString(),
                    BeginDateTime = schedule.BeginDateTime,
                    EndDateTime = schedule.EndDateTime,
                    HasClassesId = schedule.HasClasses.Id.ToString(),
                    IsProfessorPresent = schedule.IsProfessorPresent,
                    Students = studentsList,
                    Team = schedule.HasClasses.Team.Name,
                    Ufcd = $"{schedule.HasClasses.Ufcd.Reference} - {schedule.HasClasses.Ufcd.Name}"
                };
                scheduleVM.Add(sched);
            }

            return new JsonNetResult(scheduleVM);
        }

        // POST: Schedules/Update
        public async Task<ActionResult> Update(int id)
        {
            var sched = await schedRepo.GetByIdAsync(id);
            if (sched == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var form = Request.Form;

            var studentListString = form.AllKeys.Where(x => x.StartsWith("student-")).ToArray();

            var isTeacherPresent = form["isTeacherPresent"];
            if (isTeacherPresent != null)
            {
                sched.IsProfessorPresent = isTeacherPresent == "on" ? true : false;
            }

            var teamStudents = await teamRepo.GetTeamAllStudentsAsync(sched.HasClasses.TeamId);
            foreach (var student in teamStudents)
            {
                if (!studentListString.Contains($"student-{student.Id.ToString()}") ||
                    form[studentListString.Where(x => x == $"student-{student.Id.ToString()}").FirstOrDefault()] != "on")
                {
                    var studentToRemove = sched.StudentsAttended.FirstOrDefault(x => x.Id == student.Id);
                    if (studentToRemove != null)
                    {
                        sched.StudentsAttended.Remove(studentToRemove);
                    }
                }
                else
                {
                    var studentToAdd = sched.StudentsAttended.FirstOrDefault(x => x.Id == student.Id);
                    if (studentToAdd == null)
                    {
                        sched.StudentsAttended.Add(sched.HasClasses.Team.Inscriptions
                            .Where(x => x.StudentId == student.Id && x.Accepted)
                            .Select(x => x.Student)
                            .FirstOrDefault());
                    }
                }
            }
            await schedRepo.UpdateAsync(sched);

            return new HttpStatusCodeResult(200);
        }

        // GET: Schedules/Create/{teamId}
        public async Task<ActionResult> Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var hasClass = hasClassRepo.GetClassesByTeamId(id.Value);

            ViewBag.TeamId = id.Value;

            List<Ufcd> ufcds = new List<Ufcd>();
            if (hasClass != null)
            {
                foreach (var taught in hasClass)
                {
                    ufcds.Add(taught.Ufcd);
                }

                ViewBag.Ufcds = ufcds;
            }

            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            return View();
        }

        // POST: Schedules/Create/{teamId}
        [HttpPost]
        public async Task<ActionResult> Create(int id, [Bind(Include = "BeginDateTime, EndDateTime")] Schedule schedule)
        {
            var form = Request.Form;

            var ufcdId = form["ufcdList"];
            var professorId = form["professor"];
            if (ufcdId != null)
            {
                var ufcd = int.Parse(ufcdId);
                if (professorId != null)
                {
                    var professor = int.Parse(professorId);
                        
                    var hasClass = hasClassRepo.GetByTeamIdAndUfcdId(id, ufcd);
                    schedule.HasClassesId = hasClass.Id;

                    ModelState.Clear();
                    if (TryValidateModel(schedule))
                    {
                        if (schedule.BeginDateTime.CompareTo(schedule.EndDateTime) >= 0)
                        {
                            TempData["ErrorMessage"] = "A sessão não pode terminar antes de iniciar.";

                            return RedirectToAction("Create", new { id = id });
                        }

                        await schedRepo.AddAsync(schedule);
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "A sessão necessita de um início e um término válido.";

                        return RedirectToAction("Create", new { id = id });
                    }
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Por favor escolha uma UFCD";
                //ModelState.AddModelError("", "Por favor escolha uma UFCD");
                return RedirectToAction("Create", new { id = id });
            }

            return RedirectToAction("Team", new { id = id });
        }

        // POST: Schedules/Teaches?teamID={teamID}&ufcdID={ufcdID}
        [HttpPost]
        public async Task<ActionResult> Teaches(int teamID, int ufcdID)
        {
            var hasClass = hasClassRepo.GetByTeamIdAndUfcdId(teamID, ufcdID);
            if (hasClass != null)
            {
                return Json(new {
                    professor = hasClass.Professor.Profile.FullName,
                    id = hasClass.Professor.Id
                });
            }
            return Json("");
        }

        // GET: Schedules/Edit
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedule schedule = await schedRepo.GetByIdAsync(id.Value);


            var hasClass = schedule.HasClasses;

            ViewBag.TeamId = hasClass.TeamId;

            List<Ufcd> ufcds = new List<Ufcd>();
            if (hasClass != null)
            {
                foreach (var taught in hasClassRepo.GetClassesByTeamId(hasClass.TeamId))
                {
                    ufcds.Add(taught.Ufcd);
                }

                ViewBag.Ufcds = ufcds;
            }

            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            return View(schedule);
        }

        // POST: Schedules/Edit/{id}
        [HttpPost]
        public async Task<ActionResult> Edit(int id, [Bind(Include = "Id, BeginDateTime, EndDateTime")] Schedule schedule)
        {
            var form = Request.Form;

            var ufcdId = form["ufcdList"];
            var professorId = form["professor"];
            var teamId = form["teamId"];

            if (ufcdId != null)
            {
                var ufcd = int.Parse(ufcdId);
                if (teamId != null)
                {
                    var team = int.Parse(teamId);

                    if (professorId != null)
                    {
                        var hasClass = hasClassRepo.GetByTeamIdAndUfcdId(team, ufcd);
                        schedule.HasClassesId = hasClass.Id;

                        ModelState.Clear();
                        if (TryValidateModel(schedule))
                        {
                            if (schedule.BeginDateTime.CompareTo(schedule.EndDateTime) >= 0)
                            {
                                TempData["ErrorMessage"] = "A sessão não pode terminar antes de iniciar.";

                                return RedirectToAction("Edit", new { id = id });
                            }

                            await schedRepo.UpdateAsync(schedule);

                            return RedirectToAction("Team", new { id = team });
                        }
                        else
                        {
                            TempData["ErrorMessage"] = "A sessão necessita de um início e um término válido.";

                            return RedirectToAction("Edit", new { id = id });
                        }
                    }
                }

                TempData["ErrorMessage"] = "Erro inesperado! Por favor contacte o administrador.";
                //ModelState.AddModelError("", "Por favor escolha uma UFCD");
                return RedirectToAction("Edit", new { id = id });
            }
            else
            {
                TempData["ErrorMessage"] = "Por favor escolha uma UFCD";
                //ModelState.AddModelError("", "Por favor escolha uma UFCD");
                return RedirectToAction("Edit", new { id = id });
            }

            return RedirectToAction("Team", new { id = id });
        }

        // POST: Schedules/Delete/{id}
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var sched = await schedRepo.GetByIdAsync(id);
            if (sched != null)
            {
                await schedRepo.DeleteAsync(sched);
            }

            return Json("");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                profileRepo.Dispose();
                schedRepo.Dispose();
                teamRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
