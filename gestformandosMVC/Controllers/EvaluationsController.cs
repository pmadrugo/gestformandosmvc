﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.ViewModels.Evaluations;
using gestformandosMVC.ViewModels.Units;
using Microsoft.AspNet.Identity;

namespace gestformandosMVC.Controllers
{
    [Authorize]
    public class EvaluationsController : Controller
    {
        private EvaluationRepository evalRepo = new EvaluationRepository();
        private TeamRepository teamRepo = new TeamRepository();
        private UfcdRepository ufcdRepo = new UfcdRepository();
        private HasClassesRepository hasClassesRepo = new HasClassesRepository();
        private ProfileRepository profileRepo = new ProfileRepository();
        private StudentRepository studentRepo = new StudentRepository();

        // GET: Evaluations
        public async Task<ActionResult> Index()
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Evaluations/Team/{id}
        [Authorize(Roles = "SuperUser, Administrativo, Director, Formador")]
        public async Task<ActionResult> Team(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await teamRepo.GetByIdAsync(id.Value);
            if (team != null)
            {
                List<Student> teamStudents = await teamRepo.GetTeamAllStudentsAsync(id.Value);
                if (teamStudents != null)
                {
                    var evaluationVM = new GradeableStudentsTeamViewModel()
                    {
                        Team = team,
                        Students = teamStudents
                    };

                    evaluationVM.StudentEvalsPerUFCD = new List<UFCDStudentEvalUnitViewModel>();
                    foreach (var ufcd in team.Course.Ufcds)
                    {
                        var ufcdStudentEvals = new UFCDStudentEvalUnitViewModel()
                        {
                            Ufcd = ufcd
                        };

                        ufcdStudentEvals.StudentEvals = new List<StudentEvalUnitViewModel>();
                        foreach (var student in teamStudents)
                        {
                            var studentEval = new StudentEvalUnitViewModel()
                            {
                                Evaluation = evalRepo.GetEvaluationByStudentAndUfcd(student.Id, ufcd.Id),
                                Student = student
                            };

                            ufcdStudentEvals.StudentEvals.Add(studentEval);
                        }

                        var hasClass = hasClassesRepo.GetByTeamIdAndUfcdId(team.Id, ufcd.Id);
                        var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());

                        if (hasClass != null)
                        {
                            if (hasClass.Professor.ProfileId == profile.Id)
                            {
                                ufcdStudentEvals.IsTeacher = true;
                                evaluationVM.IsTeacher = true;
                            }
                        }

                        evaluationVM.StudentEvalsPerUFCD.Add(ufcdStudentEvals);
                    }

                    return View(evaluationVM);
                }
            }
            return View();
        }

        // POST: Evaluations/Evaluate?teamID={teamID}&evalID={evalID}
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "SuperUser, Administrativo, Director, Formador")]
        public async Task<ActionResult> Evaluate(int teamID, int evalID)
        {
            var evaluation = await evalRepo.GetByIdAsync(evalID);
            if (evaluation != null)
            {
                var gradeString = Request.Form["grade"].Replace('.',',');
                var grade = double.Parse(gradeString);
                var description = Request.Form["description"];

                evaluation.Grade = grade;
                evaluation.Description = description;

                await evalRepo.UpdateAsync(evaluation);

                ViewBag.SuccessMessage = "Avaliação lançada com sucesso";
            }

            // Recreate View
            Team team = await teamRepo.GetByIdAsync(teamID);
            if (team != null)
            {
                List<Student> teamStudents = await teamRepo.GetTeamAllStudentsAsync(teamID);
                if (teamStudents != null)
                {
                    var evaluationVM = new GradeableStudentsTeamViewModel()
                    {
                        Team = team,
                        Students = teamStudents
                    };

                    evaluationVM.StudentEvalsPerUFCD = new List<UFCDStudentEvalUnitViewModel>();
                    foreach (var ufcd in team.Course.Ufcds)
                    {
                        var ufcdStudentEvals = new UFCDStudentEvalUnitViewModel()
                        {
                            Ufcd = ufcd
                        };

                        ufcdStudentEvals.StudentEvals = new List<StudentEvalUnitViewModel>();
                        foreach (var student in teamStudents)
                        {
                            var studentEval = new StudentEvalUnitViewModel()
                            {
                                Evaluation = evalRepo.GetEvaluationByStudentAndUfcd(student.Id, ufcd.Id),
                                Student = student
                            };

                            ufcdStudentEvals.StudentEvals.Add(studentEval);
                        }

                        var hasClass = hasClassesRepo.GetByTeamIdAndUfcdId(team.Id, ufcd.Id);
                        var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());

                        if (hasClass != null)
                        {
                            if (hasClass.Professor.ProfileId == profile.Id)
                            {
                                ufcdStudentEvals.IsTeacher = true;
                                evaluationVM.IsTeacher = true;
                            }
                        }

                        evaluationVM.StudentEvalsPerUFCD.Add(ufcdStudentEvals);
                    }

                    return View("Team", evaluationVM);
                }
            }

            return HttpNotFound();
        }

        // POST: Evaluations/Evaluate?teamID={teamID}&studentID={studentID}&ufcdID={ufcdID}
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "SuperUser, Administrativo, Director, Formador")]
        public async Task<ActionResult> EvaluateNew(int teamID, int studentID, int ufcdID)
        {
            var grade = double.Parse(Request.Form["grade"].Replace('.',','));
            var description = Request.Form["description"];

            var evaluation = new Evaluation()
            {
                StudentId = studentID,
                UfcdId = ufcdID,
                Grade = grade,
                Description = Request.Form["description"]
            };

            await evalRepo.AddAsync(evaluation);

            // Recreate View
            Team team = await teamRepo.GetByIdAsync(teamID);
            if (team != null)
            {
                List<Student> teamStudents = await teamRepo.GetTeamAllStudentsAsync(teamID);
                if (teamStudents != null)
                {
                    var evaluationVM = new GradeableStudentsTeamViewModel()
                    {
                        Team = team,
                        Students = teamStudents
                    };

                    evaluationVM.StudentEvalsPerUFCD = new List<UFCDStudentEvalUnitViewModel>();
                    foreach (var ufcd in team.Course.Ufcds)
                    {
                        var ufcdStudentEvals = new UFCDStudentEvalUnitViewModel()
                        {
                            Ufcd = ufcd
                        };

                        ufcdStudentEvals.StudentEvals = new List<StudentEvalUnitViewModel>();
                        foreach (var student in teamStudents)
                        {
                            var studentEval = new StudentEvalUnitViewModel()
                            {
                                Evaluation = evalRepo.GetEvaluationByStudentAndUfcd(student.Id, ufcd.Id),
                                Student = student
                            };

                            ufcdStudentEvals.StudentEvals.Add(studentEval);
                        }

                        var hasClass = hasClassesRepo.GetByTeamIdAndUfcdId(team.Id, ufcd.Id);
                        var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());

                        if (hasClass != null)
                        {
                            if (hasClass.Professor.ProfileId == profile.Id)
                            {
                                ufcdStudentEvals.IsTeacher = true;
                                evaluationVM.IsTeacher = true;
                            }
                        }

                        evaluationVM.StudentEvalsPerUFCD.Add(ufcdStudentEvals);
                    }

                    return View("Team", evaluationVM);
                }
            }

            return HttpNotFound();
        }

        // GET: /Evaluations/Me
        [Authorize(Roles = "Estudante")]
        public async Task<ActionResult> Me()
        {
            var currentProfile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
            if (currentProfile == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var student = await profileRepo.GetProfileStudentAsync(currentProfile.Id);
            if (student == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var evaluations = student.Evaluations.ToList();
            if (evaluations != null)
            {
                var evaluationVM = new StudentWithEvaluationsViewModel()
                {
                    Student = currentProfile,
                    Evaluations = evaluations
                };

                return View("Details", evaluationVM);
            }

            return HttpNotFound();
        }

        // GET: /Evaluations/Details/{id}
        [Authorize(Roles = "SuperUser, Administrativo, Director, Formador")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var student = await studentRepo.GetByIdAsync(id.Value);
            if (student == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var currentProfile = student.Profile;
            var evaluations = student.Evaluations.ToList();
            if (evaluations != null)
            {
                var evaluationVM = new StudentWithEvaluationsViewModel()
                {
                    Student = currentProfile,
                    Evaluations = evaluations
                };

                return View("Details", evaluationVM);
            }

            return HttpNotFound();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                evalRepo.Dispose();
                teamRepo.Dispose();
                ufcdRepo.Dispose();
                hasClassesRepo.Dispose();
                profileRepo.Dispose();
                studentRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
