﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Data.Interfaces;

namespace gestformandosMVC.Controllers
{
    [Authorize(Roles = "SuperUser")]
    public class GendersController : Controller
    {
        private GenderRepository genderRepo = new GenderRepository();

        // GET: Genders
        public async Task<ActionResult> Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            return View(await genderRepo.GetListAsync());
        }

        // GET: Genders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Genders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] Gender gender)
        {
            if (ModelState.IsValid)
            {
                await genderRepo.AddAsync(gender);
                return RedirectToAction("Index");
            }

            return View(gender);
        }

        // GET: Genders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gender gender = await genderRepo.GetByIdAsync(id.Value);
            if (gender == null)
            {
                return HttpNotFound();
            }
            return View(gender);
        }

        // POST: Genders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] Gender gender)
        {
            if (ModelState.IsValid)
            {
                await genderRepo.UpdateAsync(gender);
                return RedirectToAction("Index");
            }
            return View(gender);
        }

        // POST: Genders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Gender gender = await genderRepo.GetByIdAsync(id);
            if (!(await genderRepo.DeleteAsync(gender)))
            {
                TempData["ErrorMessage"] = $"Este registo de momento não pode ser apagado, certifique-se que o mesmo não possui outros registos associados.";
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                genderRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
