﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using Microsoft.AspNet.Identity;
using System.Collections;
using gestformandosMVC.Helpers;

namespace gestformandosMVC.Controllers
{
    [Authorize(Roles = "SuperUser, Director, Administrativo, Estudante")]
    public class InscriptionsController : Controller
    {
        private InscriptionRepository inscRepo = new InscriptionRepository();

        // GET: Inscriptions
        public async Task<ActionResult> Index()
        {
            if (User.IsInRole("SuperUser") ||
                User.IsInRole("Director") ||
                User.IsInRole("Administrativo"))
            {
                return RedirectToAction("List");
            }
            else
            {
                return RedirectToAction("Me");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Inscriptions/List
        [Authorize(Roles = "SuperUser, Director, Administrativo")]
        public async Task<ActionResult> List()
        {
            return View(inscRepo.GetPendingInscriptions());
        }

        // GET: Inscriptions/Me
        [Authorize(Roles = "Estudante")]
        public async Task<ActionResult> Me()
        {
            using (var profileRepo = new ProfileRepository())
            {
                var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
                if (profile != null)
                {
                    var student = await profileRepo.GetProfileStudentAsync(profile.Id);
                    if (student != null)
                    {
                        var result = this.inscRepo.GetInscriptionsByStudent(student.Id);
                        return View(result);
                    }
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Inscriptions/Create
        [Authorize(Roles = "Estudante")]
        public async Task<ActionResult> Create()
        {
            using (var profileRepo = new ProfileRepository())
            {
                var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
                if (profile != null)
                {
                    var student = await profileRepo.GetProfileStudentAsync(profile.Id);
                    if (student != null)
                    {
                        ViewBag.TeamId = new SelectList(await DropDownHelper.GetSignableTeamsForStudent(student.Id), "Id", "Name");

                        return View();
                    }
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Inscriptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Estudante")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TeamId")] Inscription inscription)
        {
            using (var profileRepo = new ProfileRepository())
            {
                var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
                if (profile != null)
                {
                    var student = await profileRepo.GetProfileStudentAsync(profile.Id);
                    if (student != null)
                    {
                        inscription.StudentId = student.Id;
                        inscription.Date = DateTime.Now;

                        ModelState.Clear();
                        if (TryValidateModel(inscription))
                        {
                            if ((await inscRepo.GetListAsync()).Any(x => x.StudentId == inscription.StudentId && x.TeamId == inscription.TeamId))
                            {
                                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                            }

                            await inscRepo.AddAsync(inscription);
                            return RedirectToAction("Index");
                        }

                        ViewBag.TeamId = new SelectList(await DropDownHelper.GetSignableTeamsForStudent(student.Id), "Id", "Name");

                        return View(inscription);
                    }
                }
            }

            return View();
        }

        // GET: /Inscriptions/Approve/{id}
        [Authorize(Roles = "Director, SuperUser, Administrativo")]
        public async Task<ActionResult> Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var inscription = await inscRepo.GetByIdAsync(id.Value);
            if (inscription == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            inscription.Accepted = true;
            await inscRepo.UpdateAsync(inscription);

            ViewBag.SuccessMessage = "Aluno ingressado na turma";

            if (inscription.Team.Inscriptions.Count <= 0)
            {
                using (var teamRepo = new TeamRepository())
                {
                    var team = await teamRepo.GetByIdAsync(inscription.TeamId);
                    if (team != null)
                    {
                        team.AllowSignins = false;

                        await teamRepo.UpdateAsync(team);

                        ViewBag.SuccessMessage = "Aluno ingressado na turma. Turma fechada devido a ter sido atingido o limite de alunos.";
                    }
                }
            }

            return View("List", inscRepo.GetPendingInscriptions());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                inscRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
