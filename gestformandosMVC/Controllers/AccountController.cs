﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using gestformandosMVC.Models;
using gestformandosMVC.ViewModels;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Helpers;
using gestformandosMVC.Data.Entities;
using System.Data.Common;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using gestformandosMVC.Data.DatabaseContexts;

namespace gestformandosMVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await SignInManager.UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("", "Tentativa de login inválida.");
                return View(model);
            }
            
            if (user != null && !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                string callbackUrl = await SendEmailConfirmationTokenASync(user.Id, "Confirme a sua conta - Reenvio");

                ViewBag.ErrorMessage = "Tem de confirmar o seu e-email primeiro.";
                return View("Error");
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(user.UserName, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Tentativa de login inválida.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public async Task<ActionResult> Register()
        {
            ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

            ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
            ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

            ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(NewAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Already exists profile with that email
                if (UserManager.FindByEmail(model.Email) != null)
                {
                    ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                    ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                    ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                    ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                    ViewBag.ErrorMessage = "Já existe uma conta com este email.";

                    return View(model);
                }

                // Check if VAT Number has been inserted already
                using (var profRepo = new ProfileRepository())
                {
                    // Document and VAT checks
                    var profiles = await profRepo.GetListAsync();
                    if ((profiles.Any(profile => profile.VatNumber == model.VatNumber)) ||
                        profiles.Any(profile => profile.DocumentTypeId == model.DocumentTypeId && profile.DocumentNumber == model.DocumentNumber))
                    {
                        ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                        ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                        ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                        ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                        ModelState.AddModelError("", "Já existe uma conta com estes dados.");

                        return View(model);
                    }
                }

                // All checks out, beginning profile creation
                // Creating ASP Net base identity
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    using (var profileRepo = new ProfileRepository())
                    {
                        var newProfile = new Profile()
                        {
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            GenderId = model.GenderId,
                            Birthdate = model.Birthdate,
                            Birthplace = model.Birthplace,
                            CitizenshipId = model.CitizenshipId,
                            DocumentEmissionDate = model.DocumentEmissionDate,
                            DocumentExpirationDate = model.DocumentExpirationDate,
                            DocumentNumber = model.DocumentNumber,
                            DocumentTypeId = model.DocumentTypeId,
                            Email = model.Email,
                            NationalityId = model.NationalityId,
                            VatNumber = model.VatNumber
                        };

                        var addedAccount = await UserManager.FindByEmailAsync(model.Email);
                        newProfile.UserId = addedAccount.Id;

                        var profileResult = await profileRepo.AddAsync(newProfile);
                        if (!profileResult)
                        {
                            await UserManager.DeleteAsync(user);

                            ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                            ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                            ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                            ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                            // If we got this far, something failed, redisplay form
                            return View(model);
                        }

                        if (model.ImageURL != null)
                        {
                            var folder = "~/Content/Photos";
                            var file = $"{Guid.NewGuid().ToString()}.png";

                            var response = await FileHelper.UploadImageAsync(model.ImageURL, folder, file);

                            if (response)
                            {
                                var imagePath = string.Format("{0}/{1}", folder.Substring(1), file);
                                newProfile.ImagePath = imagePath;

                                await profileRepo.UpdateAsync(newProfile);
                            }
                        }

                        var chkRole = await UserManager.AddToRoleAsync(user.Id, "Estudante");

                        var student = await profileRepo.AddProfileToStudentAsync(newProfile.Id);

                        await UserManager.UpdateAsync(user);
                    }

                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = await SendEmailConfirmationTokenASync(user.Id, "Confirme a sua conta");

                    TempData["Message"] = "Verifique o seu email e confirme a criação da conta. É necessário confirmar antes de puder utilizar a plataforma.";

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

            ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
            ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

            ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/RegisterInternalAccount
        [Authorize(Roles = "Administrativo, Director, SuperUser")]
        public async Task<ActionResult> RegisterInternalAccount()
        {
            ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

            ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
            ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

            ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

            ViewBag.Roles = new SelectList(DropDownHelper.GetRolesNewAccount(), "Id", "Name");

            return View();
        }

        // POST: /Account/RegisterInternalAccount
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrativo, Director, SuperUser")]
        public async Task<ActionResult> RegisterInternalAccount(NewAccountViewModel model)
        {
            if (string.IsNullOrEmpty(model.ChosenRole))
            {
                ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                ViewBag.Roles = new SelectList(DropDownHelper.GetRolesNewAccount(), "Id", "Name");

                ModelState.AddModelError(string.Empty, "É necessário escolher um cargo!");

                return View();
            }


            if (ModelState.IsValid)
            {// Already exists profile with that email
                if (UserManager.FindByEmail(model.Email) != null)
                {
                    ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                    ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                    ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                    ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                    ViewBag.Roles = new SelectList(DropDownHelper.GetRolesNewAccount(), "Id", "Name");

                    ViewBag.ErrorMessage = "Já existe uma conta com este email.";

                    return View(model);
                }

                // Check if VAT Number has been inserted already
                using (var profRepo = new ProfileRepository())
                {
                    // Document and VAT checks
                    var profiles = await profRepo.GetListAsync();
                    if ((profiles.Any(profile => profile.VatNumber == model.VatNumber)) ||
                        profiles.Any(profile => profile.DocumentTypeId == model.DocumentTypeId && profile.DocumentNumber == model.DocumentNumber))
                    {
                        ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                        ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                        ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                        ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                        ViewBag.Roles = new SelectList(DropDownHelper.GetRolesNewAccount(), "Id", "Name");

                        ViewBag.ErrorMessage = "Já existe uma conta com estes dados.";

                        return View(model);
                    }
                }

                // All checks out, beginning profile creation
                // Creating ASP Net base identity
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    using (var profileRepo = new ProfileRepository())
                    {
                        var newProfile = new Profile()
                        {
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            GenderId = model.GenderId,
                            Birthdate = model.Birthdate,
                            Birthplace = model.Birthplace,
                            CitizenshipId = model.CitizenshipId,
                            DocumentEmissionDate = model.DocumentEmissionDate,
                            DocumentExpirationDate = model.DocumentExpirationDate,
                            DocumentNumber = model.DocumentNumber,
                            DocumentTypeId = model.DocumentTypeId,
                            Email = model.Email,
                            NationalityId = model.NationalityId,
                            VatNumber = model.VatNumber
                        };

                        var addedAccount = await UserManager.FindByEmailAsync(model.Email);
                        newProfile.UserId = addedAccount.Id;

                        var profileResult = await profileRepo.AddAsync(newProfile);
                        if (!profileResult)
                        {
                            await UserManager.DeleteAsync(user);

                            ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

                            ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
                            ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

                            ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

                            ViewBag.Roles = new SelectList(DropDownHelper.GetRolesNewAccount(), "Id", "Name");

                            // If we got this far, something failed, redisplay form
                            return View(model);
                        }

                        if (model.ImageURL != null)
                        {
                            var folder = "~/Content/Photos";
                            var file = $"{Guid.NewGuid().ToString()}.png";

                            var response = await FileHelper.UploadImageAsync(model.ImageURL, folder, file);

                            if (response)
                            {
                                var imagePath = string.Format("{0}/{1}", folder.Substring(1), file);
                                newProfile.ImagePath = imagePath;

                                await profileRepo.UpdateAsync(newProfile);
                            }
                        }

                        using (GestFormandosDataContext db = new GestFormandosDataContext())
                        {
                            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

                            var role = await roleManager.FindByIdAsync(model.ChosenRole);
                            var chkRole = await UserManager.AddToRoleAsync(user.Id, role.Name);

                            switch (role.Name)
                            {
                                case "Formador":
                                {
                                    var prof = await profileRepo.AddProfileToProfessorAsync(newProfile.Id);
                                } break;
                                case "Administrativo":
                                {
                                    var admin = await profileRepo.AddProfileToAdministrationAsync(newProfile.Id);
                                } break;
                                case "Estudante":
                                {
                                    var student = await profileRepo.AddProfileToStudentAsync(newProfile.Id);
                                } break;
                            }

                            await UserManager.UpdateAsync(user);
                        }
                    }

                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = await SendEmailConfirmationTokenASync(user.Id, "Confirme a sua conta");

                    TempData["Message"] = "O utilizador para que destina esta conta necessita de confirmar o seu e-mail. É necessário confirmar antes de puder utilizar a plataforma.";

                    return RedirectToAction("Index", "Home");
                }
            }

            ViewBag.GenderId = new SelectList(await DropDownHelper.GetGendersAsync(), "Id", "Name");

            ViewBag.CitizenshipId = new SelectList(await DropDownHelper.GetCitizenshipAsync(), "Id", "Citizenship");
            ViewBag.NationalityId = new SelectList(await DropDownHelper.GetNationalityAsync(), "Id", "Nationality");

            ViewBag.DocumentTypeId = new SelectList(await DropDownHelper.GetDocumentTypesAsync(), "Id", "Name");

            ViewBag.Roles = new SelectList(DropDownHelper.GetRolesNewAccount(), "Id", "Name");

            ViewBag.ErrorMessage = "É necessário escolher um cargo!";

            return View();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Reiniciar Password", $"Por favor reinicie aqui: {callbackUrl} ");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        ////
        //// POST: /Account/ExternalLogin
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Request a redirect to the external login provider
        //    return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        //}

        ////
        //// GET: /Account/SendCode
        //[AllowAnonymous]
        //public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        //{
        //    var userId = await SignInManager.GetVerifiedUserIdAsync();
        //    if (userId == null)
        //    {
        //        return View("Error");
        //    }
        //    var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
        //    var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
        //    return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        //}

        ////
        //// POST: /Account/SendCode
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> SendCode(SendCodeViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View();
        //    }

        //    // Generate the token and send it
        //    if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
        //    {
        //        return View("Error");
        //    }
        //    return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        //}

        //
        //// GET: /Account/ExternalLoginCallback
        //[AllowAnonymous]
        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        //    if (loginInfo == null)
        //    {
        //        return RedirectToAction("Login");
        //    }

        //    // Sign in the user with this external login provider if the user already has a login
        //    var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
        //    switch (result)
        //    {
        //        case SignInStatus.Success:
        //            return RedirectToLocal(returnUrl);
        //        case SignInStatus.LockedOut:
        //            return View("Lockout");
        //        case SignInStatus.RequiresVerification:
        //            return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
        //        case SignInStatus.Failure:
        //        default:
        //            // If the user does not have an account, then prompt the user to create an account
        //            ViewBag.ReturnUrl = returnUrl;
        //            ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
        //            return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
        //    }
        //}

        //
        //// POST: /Account/ExternalLoginConfirmation
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        return RedirectToAction("Index", "Manage");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            return View("ExternalLoginFailure");
        //        }
        //        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //        var result = await UserManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //            if (result.Succeeded)
        //            {
        //                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewBag.ReturnUrl = returnUrl;
        //    return View(model);
        //}

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        ////
        //// GET: /Account/ExternalLoginFailure
        //[AllowAnonymous]
        //public ActionResult ExternalLoginFailure()
        //{
        //    return View();
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private async Task<string> SendEmailConfirmationTokenASync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
                new { userId = userID, code = code }, protocol: Request.Url.Scheme);

            await UserManager.SendEmailAsync(userID, subject,
                $"Por favor confirme a sua conta acedendo a este link: {callbackUrl}");

            return callbackUrl;
        }
        #endregion
    }
}