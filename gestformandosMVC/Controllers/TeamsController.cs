﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using Microsoft.AspNet.Identity;
using gestformandosMVC.ViewModels;
using gestformandosMVC.ViewModels.Teams;
using gestformandosMVC.ViewModels.Units;

namespace gestformandosMVC.Controllers
{
    [Authorize]
    public class TeamsController : Controller
    {
        private TeamRepository teamRepo = new TeamRepository();

        // GET: Teams
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            if (User.IsInRole("Estudante"))
            {
                return RedirectToAction("Me");
            }

            if (User.Identity.IsAuthenticated)
            {
                return View((await teamRepo.GetListAsync()).OrderByDescending(team => team.Id));
            }
            else
            {
                return View(await teamRepo.GetSignableTeamsAsync());
            }
        }

        // GET: Teams/Me
        [Authorize(Roles = "Estudante")]
        public async Task<ActionResult> Me()
        {
            using (var profileRepo = new ProfileRepository())
            {
                var profile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
                var student = await profileRepo.GetProfileStudentAsync(profile.Id);

                return View("Index", await teamRepo.GetTeamsByStudentIdAsync(student.Id));
            }
        }

        // GET: Teams/Details/5
        [AllowAnonymous]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await teamRepo.GetByIdAsync(id.Value);
            if (team == null)
            {
                return HttpNotFound();
            }

            using (var hasClassRepo = new HasClassesRepository())
            {
                var teamUfcd = new TeamDetailsWithUFCDViewModel()
                {
                    Team = team
                };

                teamUfcd.UfcdTeacher = new List<UFCDTeacherUnitViewModel>();
                using (var profileRepo = new ProfileRepository())
                {
                    Profile currentProfile = null;
                    if (User.Identity.IsAuthenticated)
                    {
                        currentProfile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());
                    }
                    

                    foreach (var hasClass in hasClassRepo.GetClassesByTeamId(team.Id))
                    {
                        var ufcdUnitVM = new UFCDTeacherUnitViewModel()
                        {
                            Ufcd = hasClass.Ufcd,
                            SelectedProfessor = hasClass.Professor.Profile,
                            IsTeacher = false
                        };

                        if (currentProfile != null)
                        {
                            if (ufcdUnitVM.SelectedProfessor.Id == currentProfile.Id)
                            {
                                ufcdUnitVM.IsTeacher = true;
                            }
                        }

                        teamUfcd.UfcdTeacher.Add(ufcdUnitVM);
                    }

                    if (teamUfcd.UfcdTeacher.Any(x => x.IsTeacher))
                    {
                        teamUfcd.IsTeacher = true;
                    }

                    return View(teamUfcd);
                }
            }
        }


        [Authorize(Roles = "Formador, Administrativo, SuperUser, Director")]
        public async Task<ActionResult> DetailsStudents(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await teamRepo.GetByIdAsync(id.Value);
            if (team == null)
            {
                return HttpNotFound();
            }

            var studentList = await teamRepo.GetTeamAllStudentsAsync(team.Id);
            using (var hasClassRepo = new HasClassesRepository())
            {
                var teamUfcd = new TeamWithStudentsWithUFCDViewModel()
                {
                    Team = team,
                    Students = studentList
                };

                teamUfcd.UfcdTeacher = new List<UFCDTeacherUnitViewModel>();
                using (var profileRepo = new ProfileRepository())
                {
                    var currentProfile = await profileRepo.GetProfileByUserIDAsync(User.Identity.GetUserId());

                    foreach (var hasClass in hasClassRepo.GetClassesByTeamId(team.Id))
                    {
                        var ufcdUnitVM = new UFCDTeacherUnitViewModel()
                        {
                            Ufcd = hasClass.Ufcd,
                            SelectedProfessor = hasClass.Professor.Profile,
                            IsTeacher = false
                        };
                    
                        if (ufcdUnitVM.SelectedProfessor.Id == currentProfile.Id)
                        {
                            ufcdUnitVM.IsTeacher = true;
                        }

                        teamUfcd.UfcdTeacher.Add(ufcdUnitVM);
                    }
                    
                    if (teamUfcd.UfcdTeacher.Any(x => x.IsTeacher))
                    {
                        teamUfcd.IsTeacher = true;
                    }

                    return View(teamUfcd);
                } 
            }
        }

        // GET: Teams/Create
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Create()
        {
            using (var courseRepo = new CourseRepository())
            {
                ViewBag.CourseId = new SelectList((await courseRepo.GetListAsync()).OrderBy(x => x.Name), "Id", "Name");
            }

            return View();
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,StartDate,EndDate,Capacity,CourseId,AllowSignins")] Team team)
        {
            if (ModelState.IsValid)
            {
                await teamRepo.AddAsync(team);
                return RedirectToAction("Index");
            }

            using (var courseRepo = new CourseRepository())
            {
                ViewBag.CourseId = new SelectList(await courseRepo.GetListAsync(), "Id", "Name");
            }

            return View(team);
        }

        // GET: Teams/Edit/5
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await teamRepo.GetByIdAsync(id.Value);
            if (team == null)
            {
                return HttpNotFound();
            }

            using (var courseRepo = new CourseRepository())
            {
                ViewBag.CourseId = new SelectList((await courseRepo.GetListAsync()).OrderBy(x => x.Name), "Id", "Name");
            }

            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,StartDate,EndDate,Capacity,CourseId,AllowSignins")] Team team)
        {
            if (ModelState.IsValid)
            {
                await teamRepo.UpdateAsync(team);
                return RedirectToAction("Index");
            }

            using (var courseRepo = new CourseRepository())
            {
                ViewBag.CourseId = new SelectList((await courseRepo.GetListAsync()).OrderBy(x => x.Name), "Id", "Name");
            }

            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Team team = await teamRepo.GetByIdAsync(id);

            if (!(await teamRepo.DeleteAsync(team)))
            {
                TempData["ErrorMessage"] = $"Este registo de momento não pode ser apagado, certifique-se que o mesmo não possui outros registos associados.";
            }

            return RedirectToAction("Index");
        }

        // GET: Teams/Lectiveboard/{id}
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        public async Task<ActionResult> Lectiveboard(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = await teamRepo.GetByIdAsync(id.Value);
            if (team != null)
            {
                var ufcdUnitModels = new List<UFCDTeacherUnitViewModel>();

                using (var profileRepo = new ProfileRepository())
                {
                    foreach (var ufcd in team.Course.Ufcds)
                    {
                        var ufcdUnitModel = new UFCDTeacherUnitViewModel()
                        {
                            Ufcd = ufcd,
                            Professors = new List<Profile>()
                        };

                        foreach (var professor in ufcd.TaughtBy)
                        {
                            ufcdUnitModel.Professors.Add(professor.Profile);
                        }
                        ufcdUnitModel.Professors = ufcdUnitModel.Professors.OrderBy(x => x.FullName).ToList();
                        ufcdUnitModel.Professors.Insert(0, new Profile() { Id = 0, FirstName = "[Escolha um formador...]" });

                        ufcdUnitModels.Add(ufcdUnitModel);
                    }
                }

                using (var hasClassesRepo = new HasClassesRepository())
                {
                    var lectiveViewModel = new LectiveboardViewModel()
                    {
                        Team = team,
                        UFCDs = ufcdUnitModels,
                        CurrentSelected = hasClassesRepo.GetClassesByTeamId(team.Id)
                    };

                    return View(lectiveViewModel);
                }
            }

            return HttpNotFound();
        }

        // POST: Teams/Lectiveboard/{id}
        [HttpPost]
        [Authorize(Roles = "Administrativo, SuperUser, Director")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Lectiveboard(int id, FormCollection form)
        {
            using (var hasClassRepo = new HasClassesRepository())
            {
                foreach (var ufcd in form.AllKeys.Skip(1))
                {
                    var professorId = int.Parse(Request.Form[ufcd]);
                    if (professorId > 0)
                    {
                        int ufcdId = int.Parse(ufcd);

                        HasClasses hasClass = hasClassRepo.GetByTeamIdAndUfcdId(id, ufcdId);
                        if (hasClass != null)
                        {
                            hasClass.ProfessorId = professorId;

                            if (!await hasClassRepo.UpdateAsync(hasClass))
                            {
                                ViewBag.ErrorMessage = "Quadro lectivo não foi actualizado!";
                                ViewBag.SuccessMessage = "";
                            }
                            else
                            {
                                ViewBag.SuccessMessage = "Quadro lectivo actualizado com sucesso!";
                                ViewBag.ErrorMessage = "";
                            }
                        }
                        else
                        {
                            hasClass = new HasClasses()
                            {
                                ProfessorId = professorId,
                                UfcdId = ufcdId,
                                TeamId = id
                            };

                            if (!await hasClassRepo.AddAsync(hasClass))
                            {
                                ViewBag.ErrorMessage = "Quadro lectivo não foi actualizado!";
                                ViewBag.SuccessMessage = "";

                            }
                            else
                            {
                                ViewBag.SuccessMessage = "Quadro lectivo actualizado com sucesso!";
                                ViewBag.ErrorMessage = "";
                            }
                        }
                    }
                }

                var ufcdUnitModels = new List<UFCDTeacherUnitViewModel>();
                Team team = await teamRepo.GetByIdAsync(id);
                using (var profileRepo = new ProfileRepository())
                {
                    foreach (var ufcd in team.Course.Ufcds)
                    {
                        var ufcdUnitModel = new UFCDTeacherUnitViewModel()
                        {
                            Ufcd = ufcd,
                            Professors = new List<Profile>()
                        };

                        foreach (var professor in ufcd.TaughtBy)
                        {
                            ufcdUnitModel.Professors.Add(professor.Profile);
                        }
                        ufcdUnitModel.Professors = ufcdUnitModel.Professors.OrderBy(x => x.FullName).ToList();
                        ufcdUnitModel.Professors.Insert(0, new Profile() { Id = 0, FirstName = "[Escolha um formador...]" });

                        ufcdUnitModels.Add(ufcdUnitModel);
                    }
                }

                using (var hasClassesRepo = new HasClassesRepository())
                {
                    var lectiveViewModel = new LectiveboardViewModel()
                    {
                        Team = team,
                        UFCDs = ufcdUnitModels,
                        CurrentSelected = hasClassesRepo.GetClassesByTeamId(team.Id)
                    };

                    return View(lectiveViewModel);
                }

                return View();
            }

            return RedirectToAction("Lectiveboard");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                teamRepo.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
