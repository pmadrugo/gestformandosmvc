﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using gestformandosMVC.Helper;

namespace gestformandosMVC.Models.GestFormandos
{
    public class Student
    {
        [Key]
        public int StudentID { get; set; }

        [Display(Name = "Primeiro Nome")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(40, ErrorMessage = "O {0} deverá conter entre {2} e {1} caractéres.")]
        public string StudentFirstName { get; set; }

        [Display(Name = "Apelido")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(40, ErrorMessage = "O {0} deverá ter entre {2} e {1} caractéres.")]
        public string StudentLastName { get; set; }

        [Display(Name = "Nome Completo")] [NotMapped]
        public virtual string StudentFullName
        {
            get { return $"{StudentFirstName} {StudentLastName}"; }
        }

        [Display(Name = "Género")] [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher um {0}.")]
        [Required(ErrorMessage = "Tem que escolher um {0}.")]
        public int StudentGenderID { get; set; }
        public virtual Gender StudentGender { get; set; }

        [Display(Name = "Data de Nascimento")] [Required(ErrorMessage = "Tem que inserir uma {0}.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)] [DataType(DataType.Date)]
        [DateTimeOfAgeRange(ErrorMessage = "O aluno tem de ser maior de idade.")]
        public DateTime StudentBirthDate { get; set; }

        [Display(Name = "NIF")] [Required(ErrorMessage = "Tem que inserir um {0}")]
        [RegularExpression("^[1-35][0-9]{8,}$", ErrorMessage = "Por favor insira um {0} válido.")]
        public string StudentVatNumber { get; set; }

        [Display(Name = "Local de Nascimento")] [Required(ErrorMessage = "O {0} deverá ter entre {2} e {1} caractéres.")]
        [StringLength(40, ErrorMessage = "Tem que inserir um {0}.")]
        public string StudentBirthplace { get; set; }

        [Display(Name = "Nacionalidade")] [Required(ErrorMessage = "Tem que escolher uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher uma {0}.")] [ForeignKey("StudentNationality")]
        public int StudentNationalityID { get; set; }
        public virtual Country StudentNationality { get; set; }

        [Display(Name = "Cidadania")] [Required(ErrorMessage = "Tem que escolher uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher uma {0}.")] [ForeignKey("StudentCitizenship")]
        public int StudentCitizenshipID { get; set; }
        public virtual Country StudentCitizenship { get; set; }

        [Display(Name = "Tipo de Documento")] [Required(ErrorMessage = "Tem que escolher um {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher um {0}.")]
        public int StudentDocumentTypeID { get; set; }
        public virtual DocumentType StudentDocumentType { get; set; }

        [Display(Name = "Número do Documento")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [RegularExpression("^[0-9]{9,}$", ErrorMessage = "Por favor insira um {0} válido.")]
        [StringLength(30, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        public string StudentDocumentNumber { get; set; }

        [Display(Name = "Data de Emissão")] [DataType(DataType.Date)]
        [Required(ErrorMessage = "Por favor escolha a {0} do documento.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StudentDocumentEmissionDate { get; set; }

        [Display(Name = "Data de Validade")] [DataType(DataType.Date)]
        [Required(ErrorMessage = "Por favor escolha a {0} do documento.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StudentDocumentExpirationDate { get; set; }

        public virtual ICollection<Evaluation> StudentEvaluations { get; set; }
    }
}