﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Models.GestFormandos
{
    public class Gender
    {
        [Key]
        public int GenderID { get; set; }

        [Display(Name = "Género")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "O campo {0} deverá conter entre {2} e {1} caractéres.")]
        public string GenderName { get; set; }

        // 1 Gender - N Students
        public virtual ICollection<Student> GenderStudents { get; set; }
    }
}