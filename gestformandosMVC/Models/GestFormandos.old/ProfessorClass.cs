﻿using gestformandosMVC.Localization.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Models.GestFormandos
{
    public class ProfessorClass
    {
        [Key]
        public int ProfessorClassID { get; set; }

        [Display(Name = "UFCD", ResourceType = typeof(ProfessorClassLoc))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "UFCDRangeError", ErrorMessageResourceType = typeof(ProfessorClassLoc))]
        public int ProfessorClassUFCDID { get; set; }
        public virtual UFCD ProfessorClassUFCD { get; set; }

        [Display(Name = "Professor", ResourceType = typeof(ProfessorClassLoc))]
        [Range(1, int.MaxValue, ErrorMessageResourceName = "ProfessorRangeError", ErrorMessageResourceType = typeof(ProfessorClassLoc))]
        public int ProfessorClassProfessorID { get; set; }
        public virtual Professor ProfessorClassProfessor { get; set; }

    }
}