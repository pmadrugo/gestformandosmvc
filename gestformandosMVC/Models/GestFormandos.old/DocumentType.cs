﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Models.GestFormandos
{
    public class DocumentType
    {
        [Key]
        public int DocumentTypeID { get; set; }

        [Display(Name = "Tipo de Documento")] [Required(ErrorMessage = "O {0} necessita de um nome.")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        public string DocumentTypeName { get; set; }

        // 1 Document Type - N Students
        public virtual ICollection<Student> DocumentTypeStudents { get; set; }
    }
}