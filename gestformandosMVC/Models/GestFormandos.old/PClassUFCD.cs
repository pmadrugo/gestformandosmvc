﻿using gestformandosMVC.Localization.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Models.GestFormandos
{
    public class PClassUFCD
    {
        [Key]
        public int PClassUFCDID { get; set; }

        [Display(Name = "Professor_Class", ResourceType = typeof(ProfessorClassLoc))]
        public int PClassUFCDPClassID { get; set; }
        public virtual ProfessorClass PClassUFCDPClass { get; set; }


        public int PClassUFCDUFCDID { get; set; }
        public virtual UFCD PClassUFCDUFCD { get; set; }
    }
}