﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using gestformandosMVC.Helper;

namespace gestformandosMVC.Models.GestFormandos
{
    public class Team
    {
        [Key]
        public int TeamID { get; set; }

        [Display(Name = "Referência")] [Required(ErrorMessage = "A turma necessita de ter uma {0}.")]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "A {0} tem de ter entre {2} e {1} caractéres.")]
        public string TeamName { get; set; }

        [Display(Name = "Data de Início")] [DataType(DataType.Date)]
        [Required(ErrorMessage = "A turma tem de ter uma {0}.")]
        public DateTime TeamStartDate { get; set; }

        [Display(Name = "Data de Término")] [DataType(DataType.Date)]
        //[Required(ErrorMessageResourceName = "EndDateReqError", ErrorMessageResourceType = typeof(TeamLoc))]
        public DateTime TeamEndDate { get; set; }

        [Display(Name = "Curso")] [Required(ErrorMessage = "A turma tem de estar associada a um {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Por favor escolha um {0}.")]
        public int TeamCourseID { get; set; }
        public virtual Course TeamCourse { get; set; }
    }
}