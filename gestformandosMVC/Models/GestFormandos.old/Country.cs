﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Models.GestFormandos
{
    public class Country
    {
        [Key]
        public int CountryID { get; set; }

        [Display(Name = "País")] [Required(ErrorMessage = "O {0} tem de ter um nome.")]
        [StringLength(40, ErrorMessage = "O {0} tem que conter entre {2} e {1} caractéres")]
        public string CountryName { get; set; }

        public virtual ICollection<Student> CountryStudents { get; set; }

        public virtual ICollection<Professor> CountryProfessors { get; set; }
    }
}