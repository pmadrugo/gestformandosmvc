﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.Models.GestFormandos
{
    public class UFCD
    {
        [Key]
        public int UFCDID { get; set; }

        [Display(Name = "UFCD")] [Required(ErrorMessage = "A {0} precisa de um nome.")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "A {0} tem de ter entre {2} e {1} caractéres.")]
        public string UFCDName { get; set; }

        [Display(Name = "Referência")] [Required(ErrorMessage = "É necessário uma {0}.")]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "A {0} tem deter entre {2} e {1} caractéres.")]
        public string UFCDReference { get; set; }

        [Display(Name = "Descrição")]
        [StringLength(250, ErrorMessage = "A {0} tem de ter entre {2} e {1} caractéres.")]
        public string UFCDDescription { get; set; }

        [Display(Name = "Duração")] [DataType(DataType.Time)]
        [Required(ErrorMessage = "É necessário preencher um valor para a {0}.")]
        public TimeSpan UFCDDuration { get; set; }

        [Display(Name = "Créditos")] [Required(ErrorMessage = "É necessário preencher o valor dos {0}.")]
        [Range(0, double.MaxValue, ErrorMessage = "Os {0} não podem ser um valor negativo.")]
        public double UFCDCredits { get; set; }
    }
}