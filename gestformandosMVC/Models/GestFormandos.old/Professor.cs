﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using gestformandosMVC.Helper;

namespace gestformandosMVC.Models.GestFormandos
{
    public class Professor
    {
        [Key]
        public int ProfessorID { get; set; }

        [Display(Name = "Primeiro Nome")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(40, ErrorMessage = "O {0} deverá conter entre {2} e {1} caractéres.")]
        public string ProfessorFirstName { get; set; }

        [Display(Name = "Apelido")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [StringLength(40, ErrorMessage = "O {0} deverá ter entre {2} e {1} caractéres.")]
        public string ProfessorLastName { get; set; }

        [Display(Name = "Nome Completo")] [NotMapped]
        public virtual string ProfessorFullName
        {
            get { return $"{ProfessorFirstName} {ProfessorLastName}"; }
        }

        [Display(Name = "Género")] [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher um {0}.")]
        [Required(ErrorMessage = "Tem que escolher um {0}.")]
        public int ProfessorGenderID { get; set; }
        public virtual Gender ProfessorGender { get; set; }

        [Display(Name = "Data de Nascimento")] [Required(ErrorMessage = "Tem que inserir uma {0}.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)] [DataType(DataType.Date)]
        [DateTimeOfAgeRange(ErrorMessage = "O aluno tem de ser maior de idade.")]
        public DateTime ProfessorBirthDate { get; set; }

        [Display(Name = "NIF")] [Required(ErrorMessage = "Tem que inserir um {0}")]
        [RegularExpression("^[1-35][0-9]{8,}$", ErrorMessage = "Por favor insira um {0} válido.")]
        public string ProfessorVatNumber { get; set; }

        [Display(Name = "Local de Nascimento")] [Required(ErrorMessage = "O {0} deverá ter entre {2} e {1} caractéres.")]
        [StringLength(40, ErrorMessage = "Tem que inserir um {0}.")]
        public string ProfessorBirthplace { get; set; }

        [Display(Name = "Nacionalidade")] [Required(ErrorMessage = "Tem que escolher uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher uma {0}.")] [ForeignKey("ProfessorNationality")]
        public int ProfessorNationalityID { get; set; }
        public virtual Country ProfessorNationality { get; set; }

        [Display(Name = "Cidadania")] [Required(ErrorMessage = "Tem que escolher uma {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher uma {0}.")] [ForeignKey("ProfessorCitizenship")]
        public int ProfessorCitizenshipID { get; set; }
        public virtual Country ProfessorCitizenship { get; set; }

        [Display(Name = "Tipo de Documento")] [Required(ErrorMessage = "Tem que escolher um {0}.")]
        [Range(1, int.MaxValue, ErrorMessage = "Tem que escolher um {0}.")]
        public int ProfessorDocumentTypeID { get; set; }
        public virtual DocumentType ProfessorDocumentType { get; set; }

        [Display(Name = "Número do Documento")] [Required(ErrorMessage = "Tem que inserir um {0}.")]
        [RegularExpression("^[0-9]{9,}$", ErrorMessage = "Por favor insira um {0} válido.")]
        [StringLength(30, ErrorMessage = "O {0} tem de ter entre {2} e {1} caractéres.")]
        public string ProfessorDocumentNumber { get; set; }

        [Display(Name = "Data de Emissão")] [DataType(DataType.Date)]
        [Required(ErrorMessage = "Por favor escolha a {0} do documento.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ProfessorDocumentEmissionDate { get; set; }

        [Display(Name = "Data de Validade")] [DataType(DataType.Date)]
        [Required(ErrorMessage = "Por favor escolha a {0} do documento.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ProfessorDocumentExpirationDate { get; set; }
    }
}