﻿using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace gestformandosMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("OTU3NjdAMzEzNzJlMzEyZTMwRWV1dDhSTDZPSVd6RGR3NUMvMGcvdmF4bkZhMm9hTE1JUXI2aWhKNGhXTT0=");

            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitialRoleSetup();
            //SetupFolders();

            DebugAccounts();
            DebugData();
        }

        protected void Application_BeginRequest()
        {
            CultureInfo culture = new CultureInfo("pt-PT");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        protected void Application_EndRequest()
        {

        }

        protected void Application_End()
        {

        }

        private void InitialRoleSetup()
        {
            using (GestFormandosDataContext dbContext = new GestFormandosDataContext())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbContext));
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbContext));

                if (!roleManager.RoleExists("SuperUser"))
                {
                    roleManager.Create(
                        new IdentityRole()
                        {
                            Name = "SuperUser"
                        }
                    );

                    string adminPwd = "P@ssw0rd";

                    var user = new ApplicationUser()
                    {
                        UserName = "pedro.marcelino.madrugo@formandos.cinel.pt",
                        Email = "pedro.marcelino.madrugo@formandos.cinel.pt",
                        EmailConfirmed = true
                    };

                    var chkUser = userManager.Create(user, adminPwd);

                    if (chkUser.Succeeded)
                    {
                        var superUserAdded = userManager.AddToRole(user.Id, "SuperUser");
                    }
                }

                if (!roleManager.RoleExists("Director"))
                {
                    roleManager.Create(
                        new IdentityRole()
                        {
                            Name = "Director"
                        }
                    );
                }

                if (!roleManager.RoleExists("Administrativo"))
                {
                    roleManager.Create(
                        new IdentityRole()
                        {
                            Name = "Administrativo"
                        }
                    );
                }

                if (!roleManager.RoleExists("Formador"))
                {
                    roleManager.Create(
                        new IdentityRole()
                        {
                            Name = "Formador"
                        }
                    );
                }

                if (!roleManager.RoleExists("Estudante"))
                {
                    roleManager.Create(
                        new IdentityRole()
                        {
                            Name = "Estudante"
                        }
                    );
                }
            }
        }

        private void SetupFolders()
        {
            if (!Directory.Exists(HttpContext.Current.Server.MapPath("Content/Photos")))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("Content/Photos"));
            }
        }

        private async void DebugAccounts()
        {
            using (GestFormandosDataContext db = new GestFormandosDataContext())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

                if (roleManager.RoleExists("Administrativo"))
                {
                    if (await userManager.FindByEmailAsync("admin@teste.com") == null)
                    {
                        var admin = new ApplicationUser()
                        {
                            UserName = "admin@teste.com",
                            Email = "admin@teste.com",
                            EmailConfirmed = true
                        };

                        var chkAdmin = userManager.Create(admin, "adminn");
                        if (chkAdmin.Succeeded)
                        {
                            var adminAdded = userManager.AddToRole(admin.Id, "Administrativo");
                            using (var profileRepo = new ProfileRepository())
                            {
                                var profile = new Profile()
                                {
                                    Birthdate = new System.DateTime(1991, 05, 19),
                                    Birthplace = "Admin",
                                    DocumentEmissionDate = new System.DateTime(1991, 05, 19),
                                    DocumentExpirationDate = new System.DateTime(1991, 05, 19),
                                    DocumentNumber = "140245507",
                                    DocumentTypeId = 1,
                                    CitizenshipId = 178,
                                    Email = admin.Email,
                                    FirstName = "Admin",
                                    LastName = "Exemplo",
                                    GenderId = 1,
                                    NationalityId = 178,
                                    UserId = admin.Id,
                                    VatNumber = "274246690"
                                };

                                await profileRepo.AddAsync(profile);

                                await profileRepo.AddProfileToAdministrationAsync(profile.Id);
                            }
                        }
                    }
                }
                if (roleManager.RoleExists("Formador"))
                {
                    if (await userManager.FindByEmailAsync("formador@teste.com") == null)
                    {
                        var formador = new ApplicationUser()
                        {
                            UserName = "formador@teste.com",
                            Email = "formador@teste.com",
                            EmailConfirmed = true
                        };

                        var chkFormador = userManager.Create(formador, "formador");
                        if (chkFormador.Succeeded)
                        {
                            var formadorAdded = userManager.AddToRole(formador.Id, "formador");
                            using (var profileRepo = new ProfileRepository())
                            {
                                var profile = new Profile()
                                {
                                    Birthdate = new System.DateTime(1991, 05, 19),
                                    Birthplace = "Formador",
                                    DocumentEmissionDate = new System.DateTime(1991, 05, 19),
                                    DocumentExpirationDate = new System.DateTime(1991, 05, 19),
                                    DocumentNumber = "140245557",
                                    DocumentTypeId = 1,
                                    CitizenshipId = 178,
                                    Email = formador.Email,
                                    FirstName = "Formador",
                                    LastName = "Exemplo",
                                    GenderId = 1,
                                    NationalityId = 178,
                                    UserId = formador.Id,
                                    VatNumber = "274246694"
                                };

                                await profileRepo.AddAsync(profile);

                                await profileRepo.AddProfileToProfessorAsync(profile.Id);
                            }
                        }
                    }
                }
                if (roleManager.RoleExists("Estudante"))
                {
                    if (await userManager.FindByEmailAsync("estudante@teste.com") == null)
                    {
                        var estudante = new ApplicationUser()
                        {
                            UserName = "estudante@teste.com",
                            Email = "estudante@teste.com",
                            EmailConfirmed = true
                        };

                        var chkEstudante = userManager.Create(estudante, "estudante");
                        if (chkEstudante.Succeeded)
                        {
                            var estudanteAdded = userManager.AddToRole(estudante.Id, "estudante");
                            using (var profileRepo = new ProfileRepository())
                            {
                                var profile = new Profile()
                                {
                                    Birthdate = new System.DateTime(1991, 05, 19),
                                    Birthplace = "Estudante",
                                    DocumentEmissionDate = new System.DateTime(1991, 05, 19),
                                    DocumentExpirationDate = new System.DateTime(1991, 05, 19),
                                    DocumentNumber = "140245587",
                                    DocumentTypeId = 1,
                                    CitizenshipId = 178,
                                    Email = estudante.Email,
                                    FirstName = "Estudante",
                                    LastName = "Exemplo",
                                    GenderId = 1,
                                    NationalityId = 178,
                                    UserId = estudante.Id,
                                    VatNumber = "274246696"
                                };

                                await profileRepo.AddAsync(profile);

                                await profileRepo.AddProfileToStudentAsync(profile.Id);
                            }
                        }
                    }
                }
            }
        }

        private async void DebugData()
        {
            // UFCDs
            using (var ufcdRepo = new UfcdRepository())
            {
                if (await ufcdRepo.GetByIdAsync(1) == null)
                {
                    var ufcdProg = new Ufcd()
                    {
                        Id = 1,
                        Name = "Programação",
                        Reference = "5000",
                        Duration = 50,
                        Credits = 25,
                        Description = ""
                    };

                    var ufcdEng = new Ufcd()
                    {
                        Id = 2,
                        Name = "Engenharia de Software",
                        Reference = "5421",
                        Duration = 50,
                        Credits = 20,
                        Description = ""
                    };

                    await ufcdRepo.AddAsync(ufcdProg);
                    await ufcdRepo.AddAsync(ufcdEng);
                }
            }

            // Courses
            using (var courseRepo = new CourseRepository())
            {
                if (await courseRepo.GetByIdAsync(1) == null)
                {
                    var engSoft = new Course()
                    {
                        Id = 1,
                        Name = "Tecnologias de Programação",
                        Reference = "22",
                        Description = ""
                    };
                    var engDesign = new Course()
                    {
                        Id = 2,
                        Name = "Curso de Design",
                        Reference = "25",
                        Description = ""
                    };

                    await courseRepo.AddWithUFCDsAsync(engSoft, new string[] { "1", "2" });
                    await courseRepo.AddWithUFCDsAsync(engDesign, new string[] { "1" });
                }
            }

            // Teams
            using (var teamRepo = new TeamRepository())
            {
                if (await teamRepo.GetByIdAsync(1) == null)
                {
                    var cet37 = new Team()
                    {
                        Id = 1,
                        Name = "CET 37",
                        StartDate = new System.DateTime(2010, 05, 19),
                        EndDate = new System.DateTime(2020, 05, 19),
                        AllowSignins = true,
                        Capacity = 20,
                        CourseId = 1
                    };

                    var cet38 = new Team()
                    {
                        Id = 2,
                        Name = "CET 38",
                        StartDate = new System.DateTime(2010, 05, 20),
                        EndDate = new System.DateTime(2020, 05, 20),
                        AllowSignins = true,
                        Capacity = 25,
                        CourseId = 2
                    };

                    await teamRepo.AddAsync(cet37);
                    await teamRepo.AddAsync(cet38);
                }
            }
        }
    }
}
