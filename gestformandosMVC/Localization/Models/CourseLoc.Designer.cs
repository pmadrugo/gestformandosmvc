﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace gestformandosMVC.Localization.Models {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CourseLoc {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CourseLoc() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("gestformandosMVC.Localization.Models.CourseLoc", typeof(CourseLoc).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The description can only contain up to 250 characters..
        /// </summary>
        public static string DescriptionLengthError {
            get {
                return ResourceManager.GetString("DescriptionLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Course Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The course must have betwen 3 and 50 characters..
        /// </summary>
        public static string NameLengthError {
            get {
                return ResourceManager.GetString("NameLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The course must have a name..
        /// </summary>
        public static string NameReqError {
            get {
                return ResourceManager.GetString("NameReqError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Course Reference.
        /// </summary>
        public static string Reference {
            get {
                return ResourceManager.GetString("Reference", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The reference consists of a value between 2 and 8 characters..
        /// </summary>
        public static string ReferenceLengthError {
            get {
                return ResourceManager.GetString("ReferenceLengthError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The reference is numeric..
        /// </summary>
        public static string ReferenceRegexError {
            get {
                return ResourceManager.GetString("ReferenceRegexError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The course must have a reference. (These values can be found at http://www.catalogo.anqep.gov.pt/Qualificacoes).
        /// </summary>
        public static string ReferenceReqError {
            get {
                return ResourceManager.GetString("ReferenceReqError", resourceCulture);
            }
        }
    }
}
