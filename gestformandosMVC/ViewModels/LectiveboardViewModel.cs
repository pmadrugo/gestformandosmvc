﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.ViewModels.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels
{
    public class LectiveboardViewModel
    {
        public Team Team { get; set; }
        public List<UFCDTeacherUnitViewModel> UFCDs { get; set; }

        public List<HasClasses> CurrentSelected { get; set; }
    }
}