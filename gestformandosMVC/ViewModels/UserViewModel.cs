﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels
{
    public class UserViewModel
    {
        public string UserID { get; set; }

        [Display(Name = "Username")]
        public string Username { get; set; }

        [Display(Name = "E-Mail")]
        [DataType(DataType.EmailAddress)]
        public string EMail { get; set; }

        [Display(Name = "Cargo")]
        public RoleViewModel Role { get; set; }

        [Display(Name = "Cargos")]
        public List<RoleViewModel> Roles { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Display(Name = "Nome do Utilizador")]
        public string FullName { get { return $"{FirstName} {LastName}"; } }
    }
}