﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels
{
    public class ProfileViewModel
    {
        public Profile Profile { get; set; }

        public Student Student
        {
            get
            {
                if (this.Profile.Student != null)
                {
                    return this.Profile.Student.ElementAtOrDefault(0);
                }

                return null;
            }
        }

        public Professor Professor
        {
            get
            {
                if (this.Profile.Professor != null)
                {
                    return this.Profile.Professor.ElementAtOrDefault(0);
                }

                return null;
            }
        }

        public Administration Administration
        {
            get
            {
                if (this.Profile.Administration != null)
                {
                    return this.Profile.Administration.ElementAtOrDefault(0);
                }

                return null;
            }
        }

        public bool StudentActive { get; set; }

        public bool ProfessorActive { get; set; }

        public bool AdministrationActive { get; set; }

        public bool IsSelf { get; set; }
    }
}