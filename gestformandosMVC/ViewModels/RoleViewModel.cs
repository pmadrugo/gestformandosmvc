﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels
{
    public class RoleViewModel
    {
        public string RoleID { get; set; }

        public string Name { get; set; }
    }
}