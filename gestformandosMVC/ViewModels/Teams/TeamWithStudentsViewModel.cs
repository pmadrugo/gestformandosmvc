﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Teams
{
    public class TeamWithStudentsViewModel
    {
        public Team Team { get; set; }

        public Student StudentLabel { get; set; }
        public Profile ProfileLabel { get; set; }

        public List<Student> Students { get; set; }
    }
}