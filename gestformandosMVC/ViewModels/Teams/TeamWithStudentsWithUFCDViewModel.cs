﻿using gestformandosMVC.ViewModels.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Teams
{
    public class TeamWithStudentsWithUFCDViewModel : TeamWithStudentsViewModel
    {
        public List<UFCDTeacherUnitViewModel> UfcdTeacher { get; set; }

        public bool IsTeacher { get; set; }
    }
}