﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Units
{
    public class UFCDStudentEvalUnitViewModel
    {
        public Ufcd Ufcd { get; set; }

        public List<StudentEvalUnitViewModel> StudentEvals { get; set; }

        public bool IsTeacher { get; set; }
    }
}