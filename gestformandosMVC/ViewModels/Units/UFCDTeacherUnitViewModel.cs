﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Units
{
    public class UFCDTeacherUnitViewModel
    {
        public Ufcd Ufcd { get; set; }

        public List<Profile> Professors { get; set; }

        public Profile SelectedProfessor { get; set; }

        public bool IsTeacher { get; set; }
    }
}