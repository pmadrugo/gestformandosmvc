﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Units
{
    public class StudentEvalUnitViewModel
    {
        public Student Student { get; set; }

        public Evaluation Evaluation { get; set; }
    }
}