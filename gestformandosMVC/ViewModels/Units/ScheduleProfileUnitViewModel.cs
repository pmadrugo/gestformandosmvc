﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Units
{
    public class ScheduleProfileUnitViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get => $"{FirstName} {LastName}";  }

        public int StudentId { get; set; }

        public int ProfileId { get; set; }

        public bool Present { get; set; }
    }
}