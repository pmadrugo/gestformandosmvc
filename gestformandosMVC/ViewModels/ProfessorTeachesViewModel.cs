﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels
{
    public class ProfessorTeachesViewModel
    {
        public string FullName { get; set; }

        public Professor Professor { get; set; }

        public List<Ufcd> ProfessorCurrentUFCDs { get; set; }
    }
}