﻿using gestformandosMVC.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Evaluations
{
    public class StudentWithEvaluationsViewModel
    {
        public Profile Student { get; set; }

        public List<Evaluation> Evaluations { get; set; }
    }
}