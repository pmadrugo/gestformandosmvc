﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.ViewModels.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Evaluations
{
    public class GradeableStudentsTeamViewModel
    {
        public Team Team { get; set; }

        public Profile ProfileLabel { get; set; }
        public Student StudentLabel { get; set; }
        public Ufcd UfcdLabel { get; set; }


        public List<Student> Students { get; set; }
        public List<UFCDStudentEvalUnitViewModel> StudentEvalsPerUFCD { get; set; }

        public bool IsTeacher { get; set; }

    }
}