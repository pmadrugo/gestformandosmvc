﻿using gestformandosMVC.Data.Entities;
using gestformandosMVC.ViewModels.Units;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace gestformandosMVC.ViewModels.Schedules
{
    public class ScheduleViewModel
    {
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "hasClassesId")]
        public string HasClassesId { get; set; }

        [JsonProperty(PropertyName = "ufcd")]
        public string Ufcd { get; set; }

        [JsonProperty(PropertyName = "team")]
        public string Team { get; set; }

        [JsonProperty(PropertyName = "start")]
        public DateTime BeginDateTime { get; set; }

        [JsonProperty(PropertyName = "end")]
        public DateTime EndDateTime { get; set; }

        [JsonProperty(PropertyName = "isProfessorPresent")]
        public bool IsProfessorPresent { get; set; }

        [JsonProperty(PropertyName = "studentList")]
        public List<ScheduleProfileUnitViewModel> Students { get; set; }
    }
}