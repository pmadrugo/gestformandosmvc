﻿using gestformandosMVC.Data.DatabaseContexts;
using gestformandosMVC.Data.Entities;
using gestformandosMVC.Data.Repository;
using gestformandosMVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace gestformandosMVC.Helpers
{
    public class DropDownHelper
    {
        public static IEnumerable<IdentityRole> GetRoles()
        {
            using (var db = new GestFormandosDataContext())
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                
                var currentUserRoles = userManager.GetRoles(HttpContext.Current.User.Identity.GetUserId());
                var roles = new List<IdentityRole>();

                if (currentUserRoles.Contains("Administrativo"))
                {
                    roles.Add(roleManager.FindByName("Formador"));
                    roles.Add(roleManager.FindByName("Estudante"));
                }
                else
                {
                    roles.Clear();
                    if (currentUserRoles.Contains("Director"))
                    {
                        roles.Add(roleManager.FindByName("Administrativo"));
                        roles.Add(roleManager.FindByName("Formador"));
                        roles.Add(roleManager.FindByName("Estudante"));
                    }
                    else
                    {
                        roles.Clear();
                        if (currentUserRoles.Contains("SuperUser"))
                        {
                            roles.Add(roleManager.FindByName("Director"));
                            roles.Add(roleManager.FindByName("Administrativo"));
                            roles.Add(roleManager.FindByName("Formador"));
                            roles.Add(roleManager.FindByName("Estudante"));
                        }
                    }
                }

                roles.RemoveAll(x => currentUserRoles.Contains(x.Name));
                roles.Insert(0, 
                    new IdentityRole()
                    {
                        Id = "",
                        Name = "[Seleccione uma permissão...]"
                    }
                );

                return roles;
            }
        }

        public static IEnumerable<IdentityRole> GetRolesNewAccount()
        {
            var roles = GetRoles();
            roles.ElementAt(0).Name = "[Seleccione um cargo...]";

            return roles;
        }

        public static async Task<IEnumerable<Gender>> GetGendersAsync()
        {
            using (var genderRepo = new GenderRepository())
            {
                return (await genderRepo.GetListAsync()).OrderBy(gender => gender.Name);
            }
        }

        public static async Task<IEnumerable<Country>> GetCitizenshipAsync()
        {
            using (var countryRepo = new CountryRepository())
            {
                return (await countryRepo.GetListAsync()).OrderBy(country => country.Citizenship);
            }
        }

        public static async Task<IEnumerable<Country>> GetCountriesAsync()
        {
            using (var countryRepo = new CountryRepository())
            {
                return (await countryRepo.GetListAsync()).OrderBy(country => country.Name);
            }
        }

        public static async Task<IEnumerable<Country>> GetNationalityAsync()
        {
            using (var countryRepo = new CountryRepository())
            {
                return (await countryRepo.GetListAsync()).OrderBy(country => country.Nationality);
            }
        }

        public static async Task<IEnumerable<DocumentType>> GetDocumentTypesAsync()
        {
            using (var documentTypeRepo = new DocumentTypeRepository())
            {
                return (await documentTypeRepo.GetListAsync()).OrderBy(documentType => documentType.Name);
            }
        }

        public static async Task<IEnumerable<Ufcd>> GetUfcdsAsync()
        {
            using (var ufcdRepo = new UfcdRepository())
            {
                return (await ufcdRepo.GetListAsync()).OrderBy(ufcd => ufcd.Name);
            }
        }

        public static async Task<IEnumerable<Team>> GetSignableTeamsForStudent(int studentID)
        {
            List<Team> teamList;

            using (TeamRepository teamRepo = new TeamRepository())
            {
                teamList = await teamRepo.GetSignableTeamsAsync();

                using (InscriptionRepository inscRepo = new InscriptionRepository())
                {
                    var inscList = inscRepo.GetInscriptionsByStudent(studentID);

                    teamList = teamList.Except(teamList.Where(x => inscList.Any(y => y.TeamId == x.Id))).ToList();
                }
            }

            teamList.Insert(0, new Team() { Id = 0, Name = "[Por favor escolha uma turma...]" });

            return teamList;
        }
    }
}