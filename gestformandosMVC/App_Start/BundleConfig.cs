﻿using BundleTransformer.Core.Bundles;
using System.Web;
using System.Web.Optimization;

namespace gestformandosMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval")
                .Include("~/Scripts/jquery.validate.js")
                .Include("~/Scripts/jquery.validate.unobtrusive.js")
                .Include("~/Scripts/jquery.validate.date.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include("~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/syncfusion")
                .Include("~/Scripts/ej2/ej2.min.js"));

            bundles.Add(new CustomStyleBundle("~/Content/Bootstrap/scss")
                .Include("~/Content/Bootstrap/bootstrap.scss"));

            bundles.Add(new CustomStyleBundle("~/Content/fontawesome/scss")
                .Include("~/Content/fontawesome/fontawesome.scss")
                .Include("~/Content/fontawesome/brands.scss")
                .Include("~/Content/fontawesome/regular.scss")
                .Include("~/Content/fontawesome/solid.scss")
                .Include("~/Content/fontawesome/v4-shims.scss"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/style.css")
                .Include("~/Content/jquery-ui.css")
                .Include("~/Content/jquery-ui.structure.css")
                .Include("~/Content/jquery-ui.theme.css")
                .Include("~/Content/ej2/bootstrap4.css"));

            bundles.Add(new CustomStyleBundle("~/Content/scss")
                .Include("~/Content/site.scss"));
        }
    }
}
